<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>Hierarchy</customSettingsType>
    <description>Settings to affect the display of the donor profile report for Contact records (visual force page)</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>Case_Closed_Days__c</fullName>
        <defaultValue>30</defaultValue>
        <deprecated>false</deprecated>
        <description>Number of days that closed cases should show on the donor profile report</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of days that closed cases should show on the donor profile report</inlineHelpText>
        <label>Case Closed Days</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Contact_ID__c</fullName>
        <deprecated>false</deprecated>
        <description>The API name of the field that contains the unique identifier for the contact. This must be a field on the contact object.</description>
        <externalId>false</externalId>
        <inlineHelpText>The API name of the field that contains the unique identifier for the contact. This must be a field on the contact  object.</inlineHelpText>
        <label>Contact ID</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Giving_Years__c</fullName>
        <defaultValue>5</defaultValue>
        <deprecated>false</deprecated>
        <description>Number of years of giving should be shown. The report will show everything from the first year of the range.</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of years of giving should be shown. The report will show everything from the first year of the range.</inlineHelpText>
        <label>Giving Years</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Logo_Path__c</fullName>
        <deprecated>false</deprecated>
        <description>The file name of the desired logo stored in the logo static resource.</description>
        <externalId>false</externalId>
        <inlineHelpText>The file name of the desired logo stored in the logo static resource.</inlineHelpText>
        <label>Logo Path</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Show_Case_Section__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Toggle to unchecked to hide the case detail section of the donor profile report</description>
        <externalId>false</externalId>
        <inlineHelpText>Toggle to unchecked to hide the case detail section of the donor profile report</inlineHelpText>
        <label>Show Case Section</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Contact_Description__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Toggle to unchecked to hide the contact description section of the donor profile report</description>
        <externalId>false</externalId>
        <inlineHelpText>Toggle to unchecked to hide the contact description section of the donor profile report</inlineHelpText>
        <label>Show Contact Description</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Contact_Section__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Toggle to unchecked to hide the contact detail section of the donor profile report</description>
        <externalId>false</externalId>
        <inlineHelpText>Toggle to unchecked to hide the contact detail section of the donor profile report</inlineHelpText>
        <label>Show Contact Section</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Giving_Section__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Toggle to unchecked to hide the giving detail section of the donor profile report</description>
        <externalId>false</externalId>
        <inlineHelpText>Toggle to unchecked to hide the giving detail section of the donor profile report</inlineHelpText>
        <label>Show Giving Section</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Preference_Section__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Toggle to unchecked to hide the preference detail section of the donor profile report</description>
        <externalId>false</externalId>
        <inlineHelpText>Toggle to unchecked to hide the preference detail section of the donor profile report</inlineHelpText>
        <label>Show Preference Section</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Proposal_Section__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Toggle to unchecked to hide the moves management detail section of the donor profile report</description>
        <externalId>false</externalId>
        <inlineHelpText>Toggle to unchecked to hide the moves management detail section of the donor profile report</inlineHelpText>
        <label>Show Proposal Section</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Relationship_Section__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Toggle to unchecked to hide the relationship detail section of the donor profile report</description>
        <externalId>false</externalId>
        <inlineHelpText>Toggle to unchecked to hide the relationship detail section of the donor profile report</inlineHelpText>
        <label>Show Relationship Section</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Show_Task_Section__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Toggle to unchecked to hide the task detail section of the donor profile report</description>
        <externalId>false</externalId>
        <inlineHelpText>Toggle to unchecked to hide the task detail section of the donor profile report</inlineHelpText>
        <label>Show Task Section</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Static_Resource_Name_For_Logo__c</fullName>
        <deprecated>false</deprecated>
        <description>Stores the static resource name from where logo comes.</description>
        <externalId>false</externalId>
        <inlineHelpText>Stores the static resource name from where logo comes.</inlineHelpText>
        <label>Static Resource Name For Logo</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Task_Scheduled_Days__c</fullName>
        <defaultValue>30</defaultValue>
        <deprecated>false</deprecated>
        <description>Number of days to show closed tasks. i.e. 30 will display closed tasks for the first 30 days from when they were scheduled</description>
        <externalId>false</externalId>
        <inlineHelpText>Number of days to show closed tasks. i.e. 30 will display closed tasks for the first 30 days from when they were scheduled</inlineHelpText>
        <label>Task Scheduled Days</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Contact Donor Profile Settings</label>
    <visibility>Public</visibility>
</CustomObject>
