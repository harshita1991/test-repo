/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PaymentServicer_ValidateTransactions implements Database.AllowsCallouts, Database.Batchable<SObject>, rC_Connect.ServiceMessageDispatcher.Service {
    global PaymentServicer_ValidateTransactions() {

    }
    global void execute(Database.BatchableContext batchableContext, List<SObject> sobjectList) {

    }
    global void finish(Database.BatchableContext batchableContext) {

    }
    global static void schedule() {

    }
    global Database.QueryLocator start(Database.BatchableContext batchableContext) {
        return null;
    }
}
