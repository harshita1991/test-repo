/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class BatchUploadSchedulable implements System.Schedulable {
    global BatchUploadSchedulable() {

    }
    global void execute(System.SchedulableContext sc) {

    }
    global static String schedule() {
        return null;
    }
}
