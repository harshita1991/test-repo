<apex:page standardController="Opportunity" extensions="rC_Connect.Opportunity_ManageDeliveryAddresses" action="{!initialize}">
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" />
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.js" />
    
    <apex:sectionHeader title="Manage Delivery {!$ObjectType.rC_Bios__Address__c.LabelPlural}" subtitle="{!opportunity.Name}" />
    <apex:pageMessages />
    
    <style>
        .initial {
            background-color: #E0FFFF; 
        }
        .Image {
           height: 20px; 
           width: 20px; 
           vertical-align: middle;
        }
        .expand_heading {
            font-size: 15px; 
         }
    </style>
    
    <script type="text/javascript">
        $(document).ready(function(){
        $(".Image").click(function(){
             $(this).parent().next(".toggle_container").slideToggle("slow");
             toggleImage($(this));
        });
        $(".toggle_container").hide();
        
        var activeDeliveryMethod = "{!JSENCODE(activeDeliveryMethod)}";
    
        if(activeDeliveryMethod == 'Address') {
           $("#firstDiv").next(".toggle_container").slideToggle("slow");
           toggleImage($("#firstDiv").children(".Image"));
        }
        else if(activeDeliveryMethod == 'Email') {
           $("#secondDiv").next(".toggle_container").slideToggle("slow");
           toggleImage($("#secondDiv").children(".Image"));
        }
        else if(activeDeliveryMethod == 'InPerson') {
           $("#thirdDiv").next(".toggle_container").slideToggle("slow");
           toggleImage($("#thirdDiv").children(".Image"));
        }
        });
      
        function toggleImage(element) {
            if($(element).attr("src") == "{!URLFOR($Resource.rC_Giving__Images, 'plus.png')}")
                $(element).attr("src","{!URLFOR($Resource.rC_Giving__Images, 'minus.png')}");
            else
                $(element).attr("src","{!URLFOR($Resource.rC_Giving__Images, 'plus.png')}");
        }
    </script>
    
    <apex:form >
        <apex:pageBlock mode="MainDetail"><!-- rendered="{!OR(accountAddressList.size >0,contactAddressList.size >0)}" -->
            <apex:commandButton action="{!cancel}" value="Back" style="margin-left: 40%;" />
            
            <div class="expand_wrapper">
                <div id="firstDiv" class="expand_heading"><img src="{!URLFOR($Resource.rC_Giving__Images, 'plus.png')}" class="Image" id="idAddress"/> Address</div>
                    <div class="toggle_container" id="firstDiv">
                     <table style="width: 100%" class="list" cellpadding="0" cellspacing="0">
                        <thead class="rich-table-thead">
                            <tr class="headerRow">
                                <th></th>
                                <th>{!$ObjectType.rC_Bios__Account_Address__c.Fields.rC_Bios__Type__c.Label}</th>
                                <th>{!$ObjectType.rC_Bios__Address__c.Fields.rC_Bios__Street_Line_1__c.Label}</th>
                                <th>{!$ObjectType.rC_Bios__Address__c.Fields.rC_Bios__Street_Line_2__c.Label}</th>
                                <th>{!$ObjectType.rC_Bios__Address__c.Fields.rC_Bios__City__c.Label}</th>
                                <th>{!$ObjectType.rC_Bios__Address__c.Fields.rC_Bios__State__c.Label}</th>
                                <th>{!$ObjectType.rC_Bios__Address__c.Fields.rC_Bios__Postal_Code__c.Label}</th>
                                <th>Preferred Type</th>
                                <th>Address Type</th>
                            </tr>
                        </thead>
                        <tbody>
                            <apex:repeat var="customAccountAddress" value="{!CustomAccountAdressList}">
                            <tr class="{!if(AND(activeDeliveryMethod=='Address',opportunityLineItemUpdate.rC_Connect__Delivery_Address__c == customAccountAddress.accountAddress.rC_Bios__Address__r.Name) ,'initial',' ')}">
                                <td>
                                    <apex:commandLink value="Select" action="{!selectAccountAddress}" styleClass="actionLink">
                                        <apex:param name="selectedId" value="{!customAccountAddress.accountAddress.Id}" />
                                        <apex:param name="deliveryMethod" value="{!customAccountAddress.preferredType}" />
                                    </apex:commandLink>
                                </td>
                                <td><apex:outPutText value=" {!customAccountAddress.accountAddress.rC_Bios__Type__c}" /></td>
                                <td><apex:outPutText value=" {!customAccountAddress.accountAddress.rC_Bios__Address__r.rC_Bios__Street_Line_1__c}" /></td>
                                <td><apex:outPutText value=" {!customAccountAddress.accountAddress.rC_Bios__Address__r.rC_Bios__Street_Line_2__c}" /></td>
                                <td><apex:outPutText value=" {!customAccountAddress.accountAddress.rC_Bios__Address__r.rC_Bios__City__c}" /></td>
                                <td><apex:outPutText value=" {!customAccountAddress.accountAddress.rC_Bios__Address__r.rC_Bios__State__c}" /></td>
                                <td><apex:outPutText value=" {!customAccountAddress.accountAddress.rC_Bios__Address__r.rC_Bios__Postal_Code__c}" /></td>
                                <td><apex:outPutText value=" {!customAccountAddress.preferredType}" /></td>   
                                <td><apex:outputText value="Account : {!customAccountAddress.accountAddress.rC_Bios__Account__r.Name}" /></td>
                            </tr>
                            </apex:repeat>
                            
                            <apex:repeat var="customContactAddress" value="{!customContactAdressList}">
                            <tr class="{!if(AND(activeDeliveryMethod=='Address',opportunityLineItemUpdate.rC_Connect__Delivery_Address__c == customContactAddress.contactAddress.rC_Bios__Address__r.Name ),'initial',' ')}">
                                <td>
                                    <apex:commandLink value="Select" action="{!selectContactAddress}" styleClass="actionLink">
                                        <apex:param name="selectedId" value="{!customContactAddress.contactAddress.Id}" />
                                         <apex:param name="deliveryMethod" value="{!customContactAddress.preferredType}" />
                                    </apex:commandLink>
                                </td>
                                <td><apex:outPutText value="{!customContactAddress.contactAddress.rC_Bios__Type__c}" /></td>
                                <td><apex:outPutText value="{!customContactAddress.contactAddress.rC_Bios__Address__r.rC_Bios__Street_Line_1__c}" /></td>
                                <td><apex:outPutText value="{!customContactAddress.contactAddress.rC_Bios__Address__r.rC_Bios__Street_Line_2__c}" /></td>
                                <td><apex:outPutText value="{!customContactAddress.contactAddress.rC_Bios__Address__r.rC_Bios__City__c}" /></td>
                                <td><apex:outPutText value="{!customContactAddress.contactAddress.rC_Bios__Address__r.rC_Bios__State__c}" /></td>
                                <td><apex:outPutText value="{!customContactAddress.contactAddress.rC_Bios__Address__r.rC_Bios__Postal_Code__c}" /></td>
                                <td><apex:outputText value="{!customContactAddress.preferredType}" /></td>
                                <td><apex:outputText value="Contact : {!customContactAddress.contactAddress.rC_Bios__Contact__r.Name}" /></td>
                            </tr>
                            </apex:repeat>
                        </tbody>
                    </table>
                </div>
                
               <div id="secondDiv" class="expand_heading"><img src="{!URLFOR($Resource.rC_Giving__Images, 'plus.png')}" class="Image" id="idEmail"/> Email</div>
                <div class="toggle_container">
                    <table  class="list" cellpadding="0" cellspacing="0" width="70%">
                        <thead class="rich-table-thead">
                            <tr class="headerRow">
                                <th></th>
                                <th>Type</th>
                                <th>Contact Name</th> 
                                <th>Email Address</th>
                            </tr>
                        </thead>
                        <apex:repeat var="customEmail" value="{!customEmailList}">
                            <tr class="{!if(AND(activeDeliveryMethod=='Email',opportunityLineItemUpdate.rC_Connect__Email__c== customEmail.email) ,'initial',' ')}">
                                <td>
                                    <apex:commandLink value="Select" action="{!selectEmail}" styleClass="actionLink">
                                        <apex:param name="selectedContactId" value="{!customEmail.contact.Id}" />
                                        <apex:param name="selectEmailType" value="{!customEmail.selectEmailType}" /> 
                                    </apex:commandLink>
                                </td>
                                <td><apex:outPutText value="{!customEmail.emailType}" /></td>
                                <td><apex:outPutText value="{!customEmail.contact.FirstName +' ' + customEmail.contact.LastName}" /></td> 
                                <td><apex:outPutText value="{!customEmail.email}" /></td>
                            </tr>
                        </apex:repeat>
                    </table>  
                </div>
                
                <div id="thirdDiv" class="expand_heading"><img src="{!URLFOR($Resource.rC_Giving__Images, 'plus.png')}" id="idInPerson" class="Image"/> In Person</div>
                <div class="toggle_container">
                    <table class="list" cellpadding="0" cellspacing="0" width="70%">
                        <thead class="rich-table-thead">
                            <tr class="headerRow">
                                <th></th>
                                <th>Type</th>
                                <th>Contact Name</th> 
                                <th>Account</th>
                            </tr>
                        </thead>
                        <apex:repeat var="customInPerson" value="{!customInPersonList}">
                        <tr class="{!if(AND(activeDeliveryMethod=='InPerson',opportunityLineItemUpdate.rC_Connect__First_Name__c+ ' '+opportunityLineItemUpdate.rC_Connect__Last_Name__c== customInPerson.contact.FirstName +' ' + customInPerson.contact.LastName) ,'initial',' ')}">
                            <td>
                                <apex:commandLink value="Select" action="{!selectInPerson}" styleClass="actionLink">
                                    <apex:param name="selectedContactId" value="{!customInPerson.contact.Id}" />
                                </apex:commandLink>
                            </td>
                            <td><apex:outPutText value="{!customInPerson.contactType}" /></td>
                            <td><apex:outPutText value="{!customInPerson.contact.FirstName +' ' + customInPerson.contact.LastName}" /></td> 
                            <td><apex:outPutText value="{!customInPerson.contact.Account.Name}" /></td>
                        </tr>
                        </apex:repeat>
                    </table> 
                </div>
            </div>
        </apex:pageBlock>
    </apex:form>
</apex:page>