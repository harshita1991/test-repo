<apex:page id="pageId" controller="rC_Connect.BatchUpload_ProcessSelectedBatch">
    <apex:form id="mainForm">
        <apex:pageBlock mode="MainDetail">
            <apex:pageBlockSection collapsible="false" title="Filter Criteria :">
                <apex:outputPanel id="batchUpload-filter">
                    <div style="width: 700px;">
                        <table>
                            <tr>
                                <td colspan="3"><apex:pageMessages id="viewError" rendered="{!viewError}"/></td>
                            </tr>
                            <tr>
                                <th>Field</th>
                                <th>Operator</th>
                                <th>Value</th>
                                <th>&nbsp;</th>
                            </tr>
                            <apex:variable var="rowcount" value="{!1}"/>
                            <apex:repeat value="{!criteriaList}" var="criteria">
                                <tr>
                                    <td>
                                        <apex:actionRegion >
                                            <apex:selectList value="{!criteria.fieldName}" size="1">
                                                <apex:selectOptions value="{!fieldSelectOptionList}"/>
                                                <apex:actionSupport event="onchange" rerender="fieldValueId,selectList" action="{!resetOperatorsForCriteria}" status="setCriteriaStatus">
                                                    <apex:param name="index" value="{!rowcount}"/>
                                                </apex:actionSupport>
                                            </apex:selectList>
                                            <apex:actionStatus id="setCriteriaStatus">
                                                <apex:facet name="start">
                                                    <apex:image value="/img/loading.gif" style="vertical-align: middle; padding-left: 1em;" />
                                                </apex:facet>
                                            </apex:actionStatus>
                                        </apex:actionRegion>
                                    </td>
                                    <td>
                                        <apex:selectList value="{!criteria.operator}" size="1" id="selectList">
                                            <apex:selectOptions value="{!operators}" rendered="{!criteria.fieldName == null}"/>
                                            <apex:selectOptions value="{!criteria.operators}" rendered="{!criteria.fieldName != null}"/>
                                        </apex:selectList>
                                    </td>
                                    <td>
                                        <apex:outputPanel id="fieldValueId">
                                            <apex:inputText rendered="{!criteria.fieldName == null}"/>
                                            <apex:inputField value="{!criteria.batchUpload[criteria.fieldName]}" rendered="{!criteria.fieldName != null}" required="false"/>
                                            <apex:inputField value="{!criteria.batchUpload.rC_Connect__Giving_Close_Date__c}" rendered="{!criteria.fieldName == 'createddate'}" required="false"/>
                                        </apex:outputPanel>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td><apex:outputLabel value="AND" rendered="{!rowcount != criteriaList.size}"/></td>
                                </tr>
                                <apex:variable var="rowcount" value="{!rowcount + 1}"/>
                            </apex:repeat>
                            <tr></tr>
                            <tr>
                                <td></td>
                                <td>
                                    <apex:actionStatus id="viewRecordStatus">
                                        <apex:facet name="stop">
                                            <apex:commandButton value="View Record" action="{!getResult}" rerender="Batch_Uploads,viewError,processingError" status="viewRecordStatus"/>
                                        </apex:facet>
                                        <apex:facet name="start">
                                            <apex:outputPanel layout="none">
                                                <apex:commandButton value="View Record" disabled="true"/>
                                                <apex:image value="/img/loading.gif" style="vertical-align: middle; padding-left: 1em;" />
                                            </apex:outputPanel>
                                        </apex:facet>
                                    </apex:actionStatus>
                                </td>
                                <td><apex:commandLink action="{!initializeFieldSelectOptionList}" value="Clear Filters" rerender="batchUpload-filter" /></td>
                            </tr>
                        </table>
                    </div>
                </apex:outputPanel>
            </apex:pageBlockSection>

            <apex:pageBlockSection title="Batch Uploads" collapsible="false"  id="Batch_Uploads" columns="1">
                <apex:pageMessages rendered="{!processingError}"/>
                <apex:pageBlockSectionItem rendered="{!batchUploadsToShowList.size > 0}">
                    <apex:actionStatus id="processRecordStatus">
                        <apex:facet name="stop">
                            <apex:outputPanel >
                                <apex:commandButton value="Process All" action="{!processAll}" rerender="Batch_Uploads" status="processRecordStatus"/>
                                <apex:commandButton value="Process Selected" action="{!processSelected}" rerender="Batch_Uploads" status="processRecordStatus"/>
                            </apex:outputPanel>
                        </apex:facet>
                        <apex:facet name="start">
                            <apex:outputPanel >
                                <apex:commandButton value="Process All" disabled="true"/>
                                <apex:commandButton value="Process Selected" disabled="true"/>
                                <apex:image value="/img/loading.gif" style="vertical-align: middle; padding-left: 1em;" />
                            </apex:outputPanel>
                        </apex:facet>
                    </apex:actionStatus>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!batchUploadsToShowList.size > 0}">
                    <apex:pageBlockTable var="batchUploadsToShow" value="{!batchUploadsToShowList}">
                        <apex:column headerClass="actionColumn" headerValue="Action" styleClass="actionColumn">
                            <apex:inputCheckbox value="{!batchUploadsToShow.checked}"/>
                        </apex:column>
                        <apex:column headerValue="{!$ObjectType.rC_Connect__Batch_Upload__c.Fields.Name.Label}">
                            <apex:outputField value="{!batchUploadsToShow.batchUpload.Name}"/>
                        </apex:column>
                        <apex:repeat var="field" value="{!$ObjectType.rC_Connect__Batch_Upload__c.FieldSets.rC_Connect__BatchUpload_ProcessSelectedBatch}">
                            <apex:column headerValue="{!field.label}" rendered="{!field.label != $ObjectType.rC_Connect__Batch_Upload__c.Fields.Name.Label}">
                                <apex:outputField value="{!batchUploadsToShow.batchUpload[field]}"/>
                            </apex:column>
                        </apex:repeat>
                    </apex:pageBlockTable>
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem rendered="{!batchUploadsToShowList.size > 0}">
                    <apex:panelGrid columns="10">
                        <apex:actionRegion >
                            <apex:outputLabel value="Show" for="recordsPerPage"/>
                            <apex:selectList value="{!recordsPerPage}" size="1" id="recordsPerPage">
                                <apex:selectOptions value="{!pageSizeOptionList}"/>
                                    <apex:actionSupport event="onchange" rerender="Batch_Uploads" action="{!getResult}" status="renderPageSizeStatus"/>
                            </apex:selectList>
                            <apex:actionStatus id="renderPageSizeStatus">
                                <apex:facet name="start">
                                    <apex:image value="/img/loading.gif" style="vertical-align: middle; padding-left: 1em;" />
                                </apex:facet>
                            </apex:actionStatus>
                        </apex:actionRegion>
                        <apex:outputText value="{!showingRecord}" />
                        <apex:outputPanel />
                        <apex:actionStatus id="nevigateStatus">
                            <apex:facet name="stop">
                                <apex:outputPanel >
                                    <apex:commandLink action="{!firstPage}" value="First" rerender="Batch_Uploads" rendered="{!hasPrevious}" status="nevigateStatus"/>&nbsp;&nbsp;
                                    <apex:commandLink action="{!previous}" value="Previous" rerender="Batch_Uploads" rendered="{!hasPrevious}" status="nevigateStatus"/>&nbsp;&nbsp;
                                    <apex:commandLink action="{!next}" value="Next" rerender="Batch_Uploads" rendered="{!hasNext}" status="nevigateStatus"/>&nbsp;&nbsp;
                                    <apex:commandLink action="{!lastPage}" value="Last" rerender="Batch_Uploads" rendered="{!hasNext}" status="nevigateStatus"/>&nbsp;&nbsp;
                                </apex:outputPanel>
                            </apex:facet>
                            <apex:facet name="start">
                                <apex:image value="/img/loading.gif" style="vertical-align: middle; padding-left: 1em;" />
                            </apex:facet>
                        </apex:actionStatus>
                    </apex:panelGrid>
                </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>
</apex:page>