<apex:page standardController="rC_Connect__Batch_Upload_Matching__c" extensions="rC_Connect.BatchUploadMatching_Manage" action="{!initialize}" tabStyle="Batch_Upload_Matching__tab">
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" />
    <apex:includeScript value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.js" />
    <apex:stylesheet value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/smoothness/jquery-ui.css" />
    <apex:sectionHeader title="{!$ObjectType.rC_Connect__Batch_Upload_Matching__c.Label}" subtitle="Home" />

    <style type="text/css">
        .batchUploadMatching input[type="text"] {
            padding: 0.30em;
        }
         
        .batchUploadMatching select {
            padding: 0.30em;
        }
        
        table.batchUploadMatchingList input[type="text"] {
            width: 95% !important;
        }
        
        table.batchUploadMatchingList .actionColumn {
            color: black !important;
        }
        
        table.batchUploadMatchingList .actionColumn input[type="text"] {
            width: 1.5em !important;
            text-align: center;
        }
        
        table.batchUploadMatchingList .dataCell.modified {
            background-color: #F5DBB3 !important;
            font-weight: bold;
        }
        
        table.batchUploadMatchingList th.headerRow.short {
            width: 5em;
        }
        
        table.batchUploadMatchingList tr.dataRow {
            height: 3.5em !important;
        }
        
        .totalRow th {
            border: none !important;
        }
        
        tr td.labelCol,tr td.dataCol,tr td.data2Col {
            height: 2.5em !important;
            vertical-align: middle !important;
        }
    </style>

    <apex:form styleClass="batchUploadMatching">
        <apex:pageMessages />

        <apex:pageBlock title="Manage {!$ObjectType.rC_Connect__Batch_Upload_Matching__c.LabelPlural}" mode="maindetail">
            <apex:pageBlockButtons location="top">
                <apex:selectList value="{!batchUploadMatchingFilter.rC_Connect__Matching_Object__c}" size="1">
                    <apex:selectOption itemValue="" itemLabel="--None--" />
                    <apex:selectOption itemValue="Account" itemLabel="{!$ObjectType.Account.Label}" />
                    <apex:selectOption itemValue="Contact" itemLabel="{!$ObjectType.Contact.Label}" />
                    <apex:selectOption itemValue="Opportunity" itemLabel="{!$ObjectType.Opportunity.Label}" />
                    <apex:selectOption itemValue="rC_Giving__Payment_Method__c" itemLabel="{!$ObjectType.rC_Giving__Payment_Method__c.Label}" />
                    <apex:selectOption itemValue="Recipient" itemLabel="Recipient Contact" />
                </apex:selectList>

                <apex:commandButton value="Refresh" action="{!reloadBatchUploadMatchingList}" />
                <apex:commandButton value="Save All" action="{!updateBatchUploadMatchingList}" />
            </apex:pageBlockButtons>

            <apex:pageBlockSection columns="1" collapsible="false" rendered="{!NOT(ISBLANK(batchUploadMatchingFilter.rC_Connect__Matching_Object__c))}">
                <apex:variable var="currentGroup" value="" />

                <apex:pageBlockTable var="batchUploadMatchingUpdate" value="{!batchUploadMatchingList}" styleClass="batchUploadMatchingList">
                    <apex:column headerClass="booleanColumn actionColumn" styleClass="booleanColumn actionColumn" footerClass="booleanColumn actionColumn" rendered="{!batchUploadMatchingList.size != 0}">
                        <span class="ui-icon ui-icon-grip-dotted-vertical" style="cursor: move;"></span>
                    </apex:column>

                    <apex:column headerClass="booleanColumn actionColumn" styleClass="booleanColumn actionColumn" footerClass="booleanColumn actionColumn" rendered="{!batchUploadMatchingList.size != 0}">
                        <apex:facet name="header">
                            <apex:outputText value="Action" />
                        </apex:facet>

                        <apex:outputPanel layout="inline">
                            <apex:outputLink value="{!URLFOR($Action.Batch_Upload_Matching__c.Delete, batchUploadMatchingUpdate.Id, [retURL=$CurrentPage.Url])}" styleClass="actionLink" onclick="return confirm('Are you sure?';">Del</apex:outputLink>
                        </apex:outputPanel>
                    </apex:column>

                    <apex:column styleClass="actionColumn booleanColumn" headerClass="actionColumn booleanColumn" footerClass="actionColumn booleanColumn">
                        <apex:facet name="header">
                            <apex:outputText value="{!$ObjectType.rC_Connect__Batch_Upload_Matching__c.Fields.rC_Connect__Group_Number__c.Label}" />
                        </apex:facet>
                        
                        <apex:variable value="{!batchUploadMatchingUpdate.rC_Connect__Group_Number__c}" var="groupNum" />
                        <apex:variable var="isBlank" value="{!batchUploadMatchingUpdate.rC_Connect__Group_Number__c == null}" />
                        <apex:variable var="isNotValid" value="{!LEN(batchUploadMatchingUpdate.rC_Connect__Group_Number__c) != 2}" />
                        
                        <apex:outputPanel layout="inline" styleClass="edit">
                            <apex:selectList value="{!batchUploadMatchingUpdate.rC_Connect__Group_Number__c}" size="1" rendered="{!NOT(OR(isNotValid, isBlank))}">
                                <apex:selectOption itemValue="01" />
                                <apex:selectOption itemValue="02" />
                                <apex:selectOption itemValue="03" />
                                <apex:selectOption itemValue="04" /> 
                                <apex:selectOption itemValue="05" />
                                <apex:selectOption itemValue="06" />
                                <apex:selectOption itemValue="07" />
                                <apex:selectOption itemValue="08" />
                                <apex:selectOption itemValue="09" />
                                <apex:selectOption itemValue="10" />
                                <apex:selectOption itemValue="11" />
                                <apex:selectOption itemValue="12" />
                                <apex:selectOption itemValue="13" />
                                <apex:selectOption itemValue="14" />
                                <apex:selectOption itemValue="15" />
                            </apex:selectList>
                            
                            <apex:selectList value="{!batchUploadMatchingUpdate.rC_Connect__Group_Number__c}" size="1" rendered="{!OR(isNotValid, isBlank)}">
                                <apex:selectOption itemlabel="{!groupNum}" itemValue="{!groupNum}" /><!--  rendered="{!OR(ISBLANK(groupNum),LEN(groupNum) != 2)}"/>   rendered="{!(LEN(groupNum) != 2)}"-->
                                <apex:selectOption itemValue="01" />
                                <apex:selectOption itemValue="02" />
                                <apex:selectOption itemValue="03" />
                                <apex:selectOption itemValue="04" /> 
                                <apex:selectOption itemValue="05" />
                                <apex:selectOption itemValue="06" />
                                <apex:selectOption itemValue="07" />
                                <apex:selectOption itemValue="08" />
                                <apex:selectOption itemValue="09" />
                                <apex:selectOption itemValue="10" />
                                <apex:selectOption itemValue="11" />
                                <apex:selectOption itemValue="12" />
                                <apex:selectOption itemValue="13" />
                                <apex:selectOption itemValue="14" />
                                <apex:selectOption itemValue="15" />
                            </apex:selectList>
                        </apex:outputPanel>
                        
                        <apex:outputPanel layout="inline" styleClass="read hidden">
                            <apex:outputText value="{!batchUploadMatchingUpdate.rC_Connect__Group_Number__c}" />
                        </apex:outputPanel>

                        <apex:outputPanel layout="inline" styleClass="original hidden">
                            <apex:outputText value="{!batchUploadMatchingUpdate.rC_Connect__Group_Number__c}" />
                        </apex:outputPanel>

                        <apex:facet name="footer">
                            <apex:selectList value="{!batchUploadMatchingInsert.rC_Connect__Group_Number__c}" size="1">
                                <apex:selectOption itemValue="01" />
                                <apex:selectOption itemValue="02" />
                                <apex:selectOption itemValue="03" />
                                <apex:selectOption itemValue="04" />
                                <apex:selectOption itemValue="05" />
                                <apex:selectOption itemValue="06" />
                                <apex:selectOption itemValue="07" />
                                <apex:selectOption itemValue="08" />
                                <apex:selectOption itemValue="09" />
                                <apex:selectOption itemValue="10" />
                                <apex:selectOption itemValue="11" />
                                <apex:selectOption itemValue="12" />
                                <apex:selectOption itemValue="13" />
                                <apex:selectOption itemValue="14" />
                                <apex:selectOption itemValue="15" />
                            </apex:selectList>
                        </apex:facet>
                    </apex:column>

                    <apex:column styleClass="actionColumn booleanColumn" headerClass="actionColumn booleanColumn" footerClass="actionColumn booleanColumn">
                        <apex:facet name="header">
                            <apex:outputText value="{!$ObjectType.rC_Connect__Batch_Upload_Matching__c.Fields.rC_Connect__Order_Number__c.Label}" />
                        </apex:facet>
                        
                        <apex:variable value="{!batchUploadMatchingUpdate.rC_Connect__Order_Number__c}" var="orderNum" />
                        <apex:variable var="isBlankOrder" value="{!batchUploadMatchingUpdate.rC_Connect__Order_Number__c == null}" />
                        <apex:variable var="isNotValidOrder" value="{!LEN(batchUploadMatchingUpdate.rC_Connect__Order_Number__c) != 2}" />
                        
                        <apex:outputPanel layout="inline" styleClass="edit">
                            <apex:selectList value="{!batchUploadMatchingUpdate.rC_Connect__Order_Number__c}" size="1" rendered="{!NOT(OR(isNotValidOrder, isBlankOrder))}">
                                <apex:selectOption itemValue="01" />
                                <apex:selectOption itemValue="02" />
                                <apex:selectOption itemValue="03" />
                                <apex:selectOption itemValue="04" />
                                <apex:selectOption itemValue="05" />
                                <apex:selectOption itemValue="06" />
                                <apex:selectOption itemValue="07" />
                                <apex:selectOption itemValue="08" />
                                <apex:selectOption itemValue="09" />
                                <apex:selectOption itemValue="10" />
                                <apex:selectOption itemValue="11" />
                                <apex:selectOption itemValue="12" />
                                <apex:selectOption itemValue="13" />
                                <apex:selectOption itemValue="14" />
                                <apex:selectOption itemValue="15" />
                            </apex:selectList>
                            
                            <apex:selectList value="{!batchUploadMatchingUpdate.rC_Connect__Order_Number__c}" size="1" rendered="{!OR(isNotValidOrder, isBlankOrder)}">
                                <apex:selectOption itemlabel="{!orderNum}" itemValue="{!orderNum}" />
                                <apex:selectOption itemValue="01" />
                                <apex:selectOption itemValue="02" />
                                <apex:selectOption itemValue="03" />
                                <apex:selectOption itemValue="04" />
                                <apex:selectOption itemValue="05" />
                                <apex:selectOption itemValue="06" />
                                <apex:selectOption itemValue="07" />
                                <apex:selectOption itemValue="08" />
                                <apex:selectOption itemValue="09" />
                                <apex:selectOption itemValue="10" />
                                <apex:selectOption itemValue="11" />
                                <apex:selectOption itemValue="12" />
                                <apex:selectOption itemValue="13" />
                                <apex:selectOption itemValue="14" />
                                <apex:selectOption itemValue="15" />
                            </apex:selectList>
                        </apex:outputPanel>

                        <apex:outputPanel layout="inline" styleClass="read hidden">
                            <apex:outputText value="{!batchUploadMatchingUpdate.rC_Connect__Order_Number__c}" />
                        </apex:outputPanel>

                        <apex:outputPanel layout="inline" styleClass="original hidden">
                            <apex:outputText value="{!batchUploadMatchingUpdate.rC_Connect__Order_Number__c}" />
                        </apex:outputPanel>

                        <apex:facet name="footer">
                            <apex:selectList value="{!batchUploadMatchingInsert.rC_Connect__Order_Number__c}" size="1">
                                <apex:selectOption itemValue="01" />
                                <apex:selectOption itemValue="02" />
                                <apex:selectOption itemValue="03" />
                                <apex:selectOption itemValue="04" />
                                <apex:selectOption itemValue="05" />
                                <apex:selectOption itemValue="06" />
                                <apex:selectOption itemValue="07" />
                                <apex:selectOption itemValue="08" />
                                <apex:selectOption itemValue="09" />
                                <apex:selectOption itemValue="10" />
                                <apex:selectOption itemValue="11" />
                                <apex:selectOption itemValue="12" />
                                <apex:selectOption itemValue="13" />
                                <apex:selectOption itemValue="14" />
                                <apex:selectOption itemValue="15" />
                            </apex:selectList>
                        </apex:facet>
                    </apex:column>

                    <apex:column headerClass="" styleClass="" footerClass="">
                        <apex:facet name="header">
                            <apex:outputText value="{!$ObjectType.rC_Connect__Batch_Upload_Matching__c.Fields.rC_Connect__Matching_Field__c.Label}" />
                        </apex:facet>

                        <apex:outputPanel layout="inline" styleClass="edit">
                            <apex:inputField value="{!batchUploadMatchingUpdate.rC_Connect__Matching_Field__c}" />
                        </apex:outputPanel>

                        <apex:outputPanel layout="inline" styleClass="read hidden">
                            <apex:outputText value="{!batchUploadMatchingUpdate.rC_Connect__Matching_Field__c}" />
                        </apex:outputPanel>

                        <apex:outputPanel layout="inline" styleClass="original hidden">
                            <apex:outputText value="{!batchUploadMatchingUpdate.rC_Connect__Matching_Field__c}" />
                        </apex:outputPanel>

                        <apex:facet name="footer">
                            <apex:inputField value="{!batchUploadMatchingInsert.rC_Connect__Matching_Field__c}" />
                        </apex:facet>
                    </apex:column>

                    <apex:column headerClass="" styleClass="" footerClass="">
                        <apex:facet name="header">
                            <apex:outputText value="{!$ObjectType.rC_Connect__Batch_Upload_Matching__c.Fields.rC_Connect__Batch_Upload_Field__c.Label}" />
                        </apex:facet>

                        <apex:outputPanel layout="inline" styleClass="edit">
                            <apex:inputField value="{!batchUploadMatchingUpdate.rC_Connect__Batch_Upload_Field__c}" />
                        </apex:outputPanel>

                        <apex:outputPanel layout="inline" styleClass="read hidden">
                            <apex:outputText value="{!batchUploadMatchingUpdate.rC_Connect__Batch_Upload_Field__c}" />
                        </apex:outputPanel>

                        <apex:outputPanel layout="inline" styleClass="original hidden">
                            <apex:outputText value="{!batchUploadMatchingUpdate.rC_Connect__Batch_Upload_Field__c}" />
                        </apex:outputPanel>

                        <apex:facet name="footer">
                            <apex:inputField value="{!batchUploadMatchingInsert.rC_Connect__Batch_Upload_Field__c}" />
                        </apex:facet>
                    </apex:column>
                </apex:pageBlockTable>

                <apex:outputPanel layout="none">
                    <apex:commandButton value="Save All" action="{!updateBatchUploadMatchingList}" />
                </apex:outputPanel>
            </apex:pageBlockSection>

            <apex:pageBlockSection title="Generated Matching Criteria" columns="1" collapsible="false" rendered="{!NOT(ISBLANK(batchUploadMatchingFilter.rC_Connect__Matching_Object__c))}">
                <apex:outputPanel layout="block">
                    <apex:variable var="currentGroup" value="--None--" />

                    <apex:repeat var="batchUploadMatchingUpdate" value="{!batchUploadMatchingList}">
                        <apex:outputText value="  " rendered="{!AND(currentGroup == '--None--')}" />
                        <apex:outputText value=" AND " rendered="{!AND(currentGroup != '--None--', currentGroup == batchUploadMatchingUpdate.rC_Connect__Group_Number__c)}" />

                        <apex:outputText rendered="{!AND(currentGroup != '--None--', currentGroup != batchUploadMatchingUpdate.rC_Connect__Group_Number__c)}">
                            <br /> OR <br />
                        </apex:outputText>

                        <apex:outputText value="{!batchUploadMatchingUpdate.rC_Connect__SOQL__c}" />
                        <apex:variable var="currentGroup" value="{!batchUploadMatchingUpdate.rC_Connect__Group_Number__c}" />
                    </apex:repeat>
                </apex:outputPanel>
            </apex:pageBlockSection>
        </apex:pageBlock>
    </apex:form>

    <script type="text/javascript">
        // Toggle the edit/read fields
        $(document).ready(function() {
            // When an edit input/select field is changed, toggle modified classes
            $('.edit input,.edit select').change(function() {
                var context = $(this).closest('.dataCell');
                var value_select = $('.edit select', context).val();
                var value_input = $('.edit input', context).val();
                var value = value_select != null ? value_select : value_input;
                
                // Save the value to the read field always
                $('.read', context).text(value);
                
                // No difference to the original?
                if (value == $('.original', context).text()) {
                    $(context).removeClass('modified');
                } else {
                    $(context).addClass('modified');
                }
            });
        });
    </script>
</apex:page>