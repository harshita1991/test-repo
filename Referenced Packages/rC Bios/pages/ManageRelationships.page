<apex:page standardController="rC_Bios__Relationship__c" extensions="rC_Bios.ManageRelationships" sidebar="{!BLANKVALUE($CurrentPage.Parameters.Sidebar,'true')}">
    <apex:includeScript value="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" />
    <apex:includeScript value="{!URLFOR($Resource.rC_Bios__strathausen_dracula_js, 'js/raphael-min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.rC_Bios__strathausen_dracula_js, 'js/dracula_graffle.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.rC_Bios__strathausen_dracula_js, 'js/dracula_graph.js')}" />
    <style type="text/css">
    .nowrap {
    white-space: nowrap;
    }
    .totalRow th {
    border: none !important;
    }
    td {
    border-width: 0 0 1px 0 !important;
    }
    table.innerGrid td {
    border: none !important;
    vertical-align: middle !important;
    }
    .industryTable td {
    border: none !important;
    }
    .industryTable td.labels {
    width: 8em;
    text-align: left;
    white-space: nowrap;
    padding-left: 0;
    }
    .industryTable td.values input[type="text"] {
    width: 10em !important;
    }
    .industryTable td.values select {
    width: 10em !important;
    }
    input[type="text"] {
    padding: 0.25em;
    }
    select {
    padding: 0.25em;
    }
    textarea {
    padding: 0.25em;
    width: 100%;
    height: 95%;
    }
    .expanded td {
    height: 2em;
    vertical-align: middle !important;
    }
    table.list td {
    vertical-align: top;
    }
    </style>
    <script type="text/javascript">
    var url = location.href;
    var match = url.match(/inline=1/);
    if (match != null) {
        var newUrl = url.replace(/inline=1/, '');
        window.top.location=newUrl;
    }

    $(document).ready(function() {
        $('.positionColumn .position').click(function() {
            $(this).closest('tr').toggleClass('expandedRow');
        });

        var temp1 = $('.NewRecordSection').find('.detailList').children('tbody').children('tr');
        $(temp1).children('td').attr('colspan','1');
        $(temp1).children('td').css('vertical-align','middle');
        $(temp1).children('td').removeClass('dataCol');
        $('.MainTable').children('tbody').append(temp1);
        $('.NewRecordSection').find('.detailList').children('tbody').children('tr').remove();
        registerEvent();
    });

    registerEvent = function() {
        var primary = "{!$ObjectType.Relationship__c.Fields.Primary__c.Name}";
        $('.Employment_Employee_'+ primary +'').on('change',function() {
            togglePrimary(this);
        });
    }

    togglePrimary = function(element) {
        var isContact = {!IsContact};
        if (!isContact || !$(element).is(":checked")) {
            return;
        }
        var primary = "{!$ObjectType.Relationship__c.Fields.Primary__c.Name}";
        $('.Employment_Employee_'+ primary +'').each(function() {
            if (this != element) {
                $(this).removeAttr('Checked');
            }
        });
    }
    </script>
    <apex:sectionHeader title="{!sectionHeaderTitle}" subtitle="{!sectionHeaderSubtitle}"/>
    <apex:pageMessages />
    <apex:form id="form" rendered="{!NOT(hasFatalError)}">
        <apex:pageBlock title="Relationships">
            <apex:pageBlockButtons location="top">
                <apex:commandButton value="Save" action="{!updateRelationshipList}" />
                <apex:commandButton value="Save & Close" action="{!updateRelationshipListAndReturn}" />
                <apex:commandButton value="Cancel" action="{!returnToRecord}" />
            </apex:pageBlockButtons>

            <apex:pageBlockTable var="relationship" value="{!relationshipList}" styleClass="MainTable" id="RelationshipTable">
                <apex:column headerValue="Action" headerClass="actionColumn" styleClass="actionColumn">
                    <apex:outputLink value="{!URLFOR($Action.Relationship__c.Delete, relationship.Id, [retURL=redirectToSelf.Url])}" styleClass="actionLink">Del</apex:outputLink>
                    <apex:outputText value=" | " />
                    <apex:outputLink value="/_ui/common/history/ui/EntityHistoryFilterPage?id={!relationship.Id}" styleClass="actionLink">History</apex:outputLink>
                </apex:column>

                <apex:column headerValue="Relationship Type">
                    <apex:outputText value="{!relationship.RecordType.Name}" styleClass="record-type-name"/>
                </apex:column>

                <apex:column headerValue="Category">
                    <apex:inputField value="{!relationship.rC_Bios__Category__c}">
                        <apex:actionSupport event="onchange" oncomplete="registerEvent()" rerender="originatingRoleId,Category, givingOriginatingRoleId, relatedRoleId, givingRelatedRoleId, otherInfoId"/>
                    </apex:inputField>
                </apex:column>

                <apex:column headerValue="Originating">
                    <apex:outputField value="{!relationship.rC_Bios__Account_1__c}" rendered="{!OR(AND(NOT(ISBLANK(relationship.Account_1__r.Name)), $CurrentPage.parameters.RelatedId == relationship.rC_Bios__Contact_2__c),recordTypeAccountAccountId == relationship.RecordTypeId,AND(recordTypeAccountContactId == relationship.RecordTypeId,isAccount))}"/>
                    <apex:outputField value="{!relationship.rC_Bios__Contact_1__c}" rendered="{!OR(AND(NOT(ISBLANK(relationship.Contact_1__r.Name)),$CurrentPage.parameters.RelatedId == relationship.rC_Bios__Account_2__c),recordTypeContactContactId == relationship.RecordTypeId,AND(recordTypeAccountContactId == relationship.RecordTypeId,isContact))}"/>
                    <apex:outputField value="{!relationship.rC_Bios__Account_2__c}" rendered="{!OR(AND(NOT(ISBLANK(relationship.Account_2__r.Name)),NOT(ISBLANK(relationship.Opportunity__r.Name)),$CurrentPage.parameters.RelatedId == relationship.rC_Bios__Account_2__c),AND(recordTypeContactGivingId == relationship.RecordTypeId,isGiving))}"/>
                    <apex:outputField value="{!relationship.rC_Bios__Contact_2__c}" rendered="{!OR(AND(NOT(ISBLANK(relationship.Contact_2__r.Name)),NOT(ISBLANK(relationship.Opportunity__r.Name)),$CurrentPage.parameters.RelatedId == relationship.rC_Bios__Contact_2__c),AND(recordTypeAccountGivingId == relationship.RecordTypeId,isGiving))}"/>
                    <apex:outputField value="{!relationship.rC_Bios__Opportunity__c}" rendered="{!OR(AND(NOT(ISBLANK(relationship.Opportunity__r.Name)),$CurrentPage.parameters.RelatedId == relationship.rC_Bios__Opportunity__c),AND(recordTypeAccountGivingId == relationship.RecordTypeId,isGiving),AND(recordTypeContactGivingId == relationship.RecordTypeId,isGiving))}"/>
                </apex:column>

                <apex:column headerValue="Originating Role">
                    <apex:outputPanel layout="none">
                        <apex:inputField value="{!relationship.rC_Bios__Role_1__c}" id="originatingRoleId" rendered="{!NOT(isGiving)}">
                            <apex:actionSupport event="onchange" oncomplete="registerEvent();" rerender="originatingRoleId,otherInfoId" />
                        </apex:inputField>
                        <apex:inputField value="{!relationship.rC_Bios__Role_2__c}" id="givingOriginatingRoleId" rendered="{!isGiving}"/>
                    </apex:outputPanel>
                </apex:column>

                <apex:column headerValue="Related">
                    <apex:outputField value="{!relationship.rC_Bios__Account_2__c}" rendered="{!OR(AND(NOT(ISBLANK(relationship.Account_2__r.Name)),ISBLANK(relationship.Opportunity__r.Name), $CurrentPage.parameters.RelatedId == relationship.rC_Bios__Account_2__c), AND(NOT(ISBLANK(relationship.Opportunity__r.Name)), $CurrentPage.parameters.RelatedId == relationship.rC_Bios__Opportunity__c),recordTypeAccountAccountId == relationship.RecordTypeId,AND(recordTypeAccountContactId == relationship.RecordTypeId,isContact))}"/>
                    <apex:outputField value="{!relationship.rC_Bios__Contact_2__c}" rendered="{!OR(AND(NOT(ISBLANK(relationship.Contact_2__r.Name)),ISBLANK(relationship.Opportunity__r.Name),$CurrentPage.parameters.RelatedId == relationship.rC_Bios__Contact_2__c), AND(NOT(ISBLANK(relationship.Opportunity__r.Name)), $CurrentPage.parameters.RelatedId == relationship.rC_Bios__Opportunity__c),recordTypeContactContactId == relationship.RecordTypeId,AND(recordTypeAccountContactId == relationship.RecordTypeId,isAccount))}"/>
                    <apex:outputField value="{!relationship.rC_Bios__Opportunity__c}" rendered="{!OR(AND(NOT(ISBLANK(relationship.Opportunity__r.Name)),$CurrentPage.parameters.RelatedId == relationship.rC_Bios__Opportunity__c, NOT(isGiving)),AND(recordTypeAccountGivingId == relationship.RecordTypeId,isAccount),AND(recordTypeContactGivingId == relationship.RecordTypeId,isContact))}"/>
                </apex:column>

                <apex:column headerValue="Related Role">
                    <apex:inputField value="{!relationship.rC_Bios__Role_2__c}" id="relatedRoleId" rendered="{!NOT(isGiving)}">
                        <apex:actionSupport event="onchange" oncomplete="registerEvent()" rerender="originatingRoleId,otherInfoId"/>
                    </apex:inputField>
                    <apex:inputField value="{!relationship.rC_Bios__Role_1__c}" id="givingRelatedRoleId" rendered="{!isGiving}"/>
                </apex:column>

                <!-- start of all the fieldset logic -->
                <apex:column headerValue="Other Information" styleClass="nowrap" Id="otherInfoId">
                    <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_AccountAccount}" rendered="{!AND(recordTypeAccountAccountId == relationship.RecordTypeId)}">
                        <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                            <apex:outputText value="{!field.Label}" />
                            <apex:inputField value="{!relationship[field]}" />
                        </apex:panelGrid>
                    </apex:repeat>

                    <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_AccountContact}" rendered="{!AND(recordTypeAccountContactId == relationship.RecordTypeId)}">
                        <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                            <apex:outputText value="{!field.Label}" />
                            <apex:inputField value="{!relationship[field]}" />
                        </apex:panelGrid>
                    </apex:repeat>

                    <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_ContactContact}" rendered="{!AND(recordTypeContactContactId == relationship.RecordTypeId)}">
                        <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                            <apex:outputText value="{!field.Label}" />
                            <apex:inputField value="{!relationship[field]}" />
                        </apex:panelGrid>
                    </apex:repeat>

                    <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_AccountGiving}" rendered="{!AND(recordTypeAccountGivingId == relationship.RecordTypeId)}">
                        <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                            <apex:outputText value="{!field.Label}" />
                            <apex:inputField value="{!relationship[field]}" />
                        </apex:panelGrid>
                    </apex:repeat>

                    <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_ContactGiving}" rendered="{!AND(recordTypeContactGivingId == relationship.RecordTypeId)}">
                        <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                            <apex:outputText value="{!field.Label}" />
                            <apex:inputField value="{!relationship[field]}" />
                        </apex:panelGrid>
                    </apex:repeat>

                    <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_CategoryBoard}" rendered="{!AND(relationship.rC_Bios__Category__c == 'Board')}">
                        <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                            <apex:outputText value="{!field.Label}" />
                            <apex:inputField value="{!relationship[field]}" />
                        </apex:panelGrid>
                    </apex:repeat>

                    <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_CategoryCorporate}" rendered="{!AND(relationship.rC_Bios__Category__c == 'Corporate')}">
                        <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                            <apex:outputText value="{!field.Label}" />
                            <apex:inputField value="{!relationship[field]}" />
                        </apex:panelGrid>
                    </apex:repeat>

                    <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_CategoryEducation}" rendered="{!AND(relationship.rC_Bios__Category__c == 'Education')}">
                        <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                            <apex:outputText value="{!field.Label}" />
                            <apex:inputField value="{!relationship[field]}" />
                        </apex:panelGrid>
                    </apex:repeat>

                    <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_CategoryEmployment}" rendered="{!AND(relationship.rC_Bios__Category__c == 'Employment')}">
                    <apex:variable var="matchingRatioType" value="{!$ObjectType.rC_Bios__Relationship__c.Fields.rC_Bios__Matching_Ratio_Type__c.Name}" />
                        <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                            <apex:outputText value="{!field.Label}" />
                            <apex:inputField value="{!relationship[field]}" styleClass="{!relationship.rC_Bios__Category__c}_{!IF(relationship.rC_Bios__Contact_2__c == $CurrentPage.parameters.RelatedId,relationship.rC_Bios__Role_2__c,relationship.rC_Bios__Role_1__c)}_{!field}" rendered="{!IF(field != matchingRatioType,true,false)}" />

                            <apex:inputField value="{!relationship[field]}" rendered="{!IF(AND(field == matchingRatioType,accountToMatchingRatioMap[relationship.rC_Bios__Account_2__c] == NULL,relationship.rC_Bios__Account_2__c != NULL),true,false)}" />
                            <apex:selectList size="1" value="{!relationship[field]}" rendered="{!IF(AND(field == matchingRatioType,accountToMatchingRatioMap[relationship.rC_Bios__Account_2__c] != NULL ,relationship.rC_Bios__Account_2__c != NULL,relationship.rC_Bios__Role_2__c == 'Employer'),true,false)}" >
                                    <apex:selectOptions value="{!accountToMatchingRatioMap[relationship.rC_Bios__Account_2__c]}"/>
                            </apex:selectList>

                            <apex:inputField value="{!relationship[field]}" rendered="{!IF(AND(field == matchingRatioType,accountToMatchingRatioMap[relationship.rC_Bios__Account_1__c] == NULL,relationship.rC_Bios__Account_1__c != NULL),true,false)}" />
                            <apex:selectList size="1" value="{!relationship[field]}" rendered="{!IF(AND(field == matchingRatioType,accountToMatchingRatioMap[relationship.rC_Bios__Account_1__c] != NULL ,relationship.rC_Bios__Account_1__c != NULL,relationship.rC_Bios__Role_1__c == 'Employer'),true,false)}" >
                                <apex:selectOptions value="{!accountToMatchingRatioMap[relationship.rC_Bios__Account_1__c]}"/>
                            </apex:selectList>
                        </apex:panelGrid>
                    </apex:repeat>

                    <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_CategoryFamily}" rendered="{!AND(relationship.rC_Bios__Category__c == 'Family')}">
                        <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                            <apex:outputText value="{!field.Label}" />
                            <apex:inputField value="{!relationship[field]}" />
                        </apex:panelGrid>
                    </apex:repeat>

                    <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_CategoryOther}" rendered="{!AND(relationship.rC_Bios__Category__c == 'Other')}">
                        <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                            <apex:outputText value="{!field.Label}" />
                            <apex:inputField value="{!relationship[field]}" />
                        </apex:panelGrid>
                    </apex:repeat>
                </apex:column>
            </apex:pageBlockTable>

            <!-- New Record -->
            <apex:outputPanel styleclass="NewRecordSection" id="NewRecord">
                <apex:pageBlockSection columns="10" id="NewRecordSection" html-class="NewRecordSection">
                    <apex:outputPanel layout="block" id="buttons" style="width:95px;">
                        <apex:commandButton value="Add" action="{!insertRelationship}" disabled="{!ISBLANK(relationshipInsert.RecordTypeId)}" />
                        <apex:commandButton value="Cancel" action="{!getRedirectToSelf}" disabled="{!ISBLANK(relationshipInsert.RecordTypeId)}" />
                    </apex:outputPanel>

                    <apex:outputPanel id="recTypeColumn">
                        <apex:selectList size="1" value="{!relationshipInsert.RecordTypeId}">
                            <apex:selectOption itemValue="" itemLabel="--None--" />
                            <apex:selectOption itemValue="{!recordTypeAccountAccountId}" itemLabel="Account - Account" rendered="{!isAccount}"/>
                            <apex:selectOption itemValue="{!recordTypeAccountContactId}" itemLabel="Account - Contact" rendered="{!isAccount || isContact}"/>
                            <apex:selectOption itemValue="{!recordTypeAccountGivingId}" itemLabel="Account - Giving" rendered="{!isAccount || isGiving}"/>
                            <apex:selectOption itemValue="{!recordTypeContactContactId}" itemLabel="Contact - Contact" rendered="{!isContact}"/>
                            <apex:selectOption itemValue="{!recordTypeContactGivingId}" itemLabel="Contact - Giving" rendered="{!isContact || isGiving}"/>
                            <apex:actionSupport event="onchange" rerender="buttons,categoryColumn,originatingRecordNameColumn,originatingRoleColumn,relatedRecordColumn,relatedRoleColumn, otherInformationColumn" status="relationshipInsert_relatedId"/>
                        </apex:selectList>
                    </apex:outputPanel>

                    <apex:outputPanel >
                        <apex:inputField value="{!relationshipInsert.rC_Bios__Category__c}" id="categoryColumn">
                            <apex:actionSupport action="{!populateMatchingRatioType}" event="onchange" oncomplete="registerEvent()" rerender="buttons,categoryColumn,originatingRecordNameColumn,originatingRoleColumn,givingOriginatingRoleColumn,relatedRecordColumn,relatedRoleColumn, otherInformationColumn" />
                        </apex:inputField>
                    </apex:outputPanel>

                    <apex:outputPanel >
                        <apex:outputText value="{!originatingRecord['Name']}" id="originatingRecordNameColumn" />
                    </apex:outputPanel>

                    <apex:outputPanel layout="block">
                        <apex:inputField value="{!relationshipInsert.rC_Bios__Role_1__c}" id="originatingRoleColumn" rendered="{!NOT(isGiving)}">
                            <apex:actionSupport event="onchange" oncomplete="registerEvent()" rerender="originatingRoleColumn,otherInformationColumn" />
                        </apex:inputField>
                        <apex:inputField value="{!relationshipInsert.rC_Bios__Role_2__c}" id="givingOriginatingRoleColumn" rendered="{!isGiving}"/>
                    </apex:outputPanel>

                    <apex:outputPanel id="relatedRecordColumn">
                        <apex:actionStatus id="relationshipInsert_relatedId">
                            <apex:facet name="stop">
                                <apex:outputPanel layout="none">
                                    <apex:inputField value="{!relationshipInsert.rC_Bios__Account_2__c}" rendered="{!recordTypeAccountAccountId == relationshipInsert.RecordTypeId}" >
                                        <apex:actionSupport action="{!populateMatchingRatioType}" status="relationshipInsert_relatedId" event="onchange" rerender="otherInformationColumn">
                                        </apex:actionSupport>
                                    </apex:inputField>
                                    <apex:inputField value="{!relationshipInsert.rC_Bios__Account_2__c}" rendered="{!AND(recordTypeAccountContactId == relationshipInsert.RecordTypeId, isContact)}">
                                        <apex:actionSupport action="{!populateMatchingRatioType}" status="relationshipInsert_relatedId" event="onchange" rerender="otherInformationColumn">
                                        </apex:actionSupport>
                                    </apex:inputField>
                                    <apex:inputField value="{!relationshipInsert.rC_Bios__Contact_2__c}" rendered="{!AND(recordTypeAccountContactId == relationshipInsert.RecordTypeId, isAccount)}" />
                                    <apex:inputField value="{!relationshipInsert.rC_Bios__Contact_2__c}" rendered="{!recordTypeContactContactId == relationshipInsert.RecordTypeId}" />
                                    <apex:inputField value="{!relationshipInsert.rC_Bios__Opportunity__c}" rendered="{!OR(AND(recordTypeAccountGivingId == relationshipInsert.RecordTypeId, isAccount), AND(recordTypeContactGivingId == relationshipInsert.RecordTypeId, isContact))}" />

                                    <apex:inputField value="{!relationshipInsert.rC_Bios__Account_2__c}" rendered="{!AND(recordTypeAccountGivingId == relationshipInsert.RecordTypeId, isGiving)}" />
                                    <apex:inputField value="{!relationshipInsert.rC_Bios__Contact_2__c}" rendered="{!AND(recordTypeContactGivingId == relationshipInsert.RecordTypeId, isGiving)}" />

                                </apex:outputPanel>
                            </apex:facet>

                            <apex:facet name="start">
                                <apex:image value="/img/loading.gif" style="vertical-align: middle;" />
                            </apex:facet>
                        </apex:actionStatus>
                    </apex:outputPanel>

                    <apex:outputPanel layout="none" id="relatedRoleColumn">
                        <apex:inputField value="{!relationshipInsert.rC_Bios__Role_2__c}" rendered="{!NOT(isGiving)}"/>
                        <apex:inputField value="{!relationshipInsert.rC_Bios__Role_1__c}" rendered="{!isGiving}"/>
                    </apex:outputPanel>

                    <apex:outputPanel id="otherInformationColumn">
                        <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_AccountAccount}" rendered="{!AND(recordTypeAccountAccountId == relationshipInsert.RecordTypeId)}">
                            <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                                <apex:outputText value="{!field.Label}" />
                                <apex:inputField value="{!relationshipInsert[field]}" />
                            </apex:panelGrid>
                        </apex:repeat>

                        <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_AccountContact}" rendered="{!AND(recordTypeAccountContactId == relationshipInsert.RecordTypeId)}">
                            <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                                <apex:outputText value="{!field.Label}" />
                                <apex:inputField value="{!relationshipInsert[field]}" />
                            </apex:panelGrid>
                        </apex:repeat>

                        <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_ContactContact}" rendered="{!AND(recordTypeContactContactId == relationshipInsert.RecordTypeId)}">
                            <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                                <apex:outputText value="{!field.Label}" />
                                <apex:inputField value="{!relationshipInsert[field]}" />
                            </apex:panelGrid>
                        </apex:repeat>

                        <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_AccountGiving}" rendered="{!AND(recordTypeAccountGivingId == relationshipInsert.RecordTypeId)}">
                            <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                                <apex:outputText value="{!field.Label}" />
                                <apex:inputField value="{!relationshipInsert[field]}" />
                            </apex:panelGrid>
                        </apex:repeat>

                        <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_ContactGiving}" rendered="{!AND(recordTypeContactGivingId == relationshipInsert.RecordTypeId)}">
                            <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                                <apex:outputText value="{!field.Label}" />
                                <apex:inputField value="{!relationshipInsert[field]}" />
                            </apex:panelGrid>
                        </apex:repeat>

                        <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_CategoryBoard}" rendered="{!AND(relationshipInsert.rC_Bios__Category__c == 'Board')}">
                            <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                                <apex:outputText value="{!field.Label}" />
                                <apex:inputField value="{!relationshipInsert[field]}" />
                            </apex:panelGrid>
                        </apex:repeat>

                        <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_CategoryCorporate}" rendered="{!AND(relationshipInsert.rC_Bios__Category__c == 'Corporate')}">
                            <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                                <apex:outputText value="{!field.Label}" />
                                <apex:inputField value="{!relationshipInsert[field]}" />
                            </apex:panelGrid>
                        </apex:repeat>

                        <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_CategoryEducation}" rendered="{!AND(relationshipInsert.rC_Bios__Category__c == 'Education')}">
                            <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                                <apex:outputText value="{!field.Label}" />
                                <apex:inputField value="{!relationshipInsert[field]}" />
                            </apex:panelGrid>
                        </apex:repeat>

                        <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_CategoryEmployment}" rendered="{!AND(relationshipInsert.rC_Bios__Category__c == 'Employment')}">
                        <apex:variable var="currentField" value="{!$ObjectType.rC_Bios__Relationship__c.Fields.rC_Bios__Matching_Ratio_Type__c.Name}" />
                         <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                                <apex:outputText value="{!field.Label}" />
                                <apex:inputField value="{!relationshipInsert[field]}" styleClass="{!relationshipInsert.rC_Bios__Category__c}_{!relationshipInsert.rC_Bios__Role_1__c}_{!field}" rendered="{!IF(field != currentField,true,false)}" />
                                <apex:inputField value="{!relationshipInsert[field]}" rendered="{!IF(AND(field == currentField, matchingRatioTypeOptions == NULL),true,false)}" />
                                <apex:selectList size="1" value="{!relationshipInsert[field]}" rendered="{!IF(AND(field == currentField, matchingRatioTypeOptions != NULL),true,false)}" >
                                    <apex:selectOptions value="{!matchingRatioTypeOptions}"/>
                                </apex:selectList>
                            </apex:panelGrid>
                        </apex:repeat>

                        <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_CategoryFamily}" rendered="{!AND(relationshipInsert.rC_Bios__Category__c == 'Family')}">
                            <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                                <apex:outputText value="{!field.Label}" />
                                <apex:inputField value="{!relationshipInsert[field]}" />
                            </apex:panelGrid>
                        </apex:repeat>

                        <apex:repeat var="field" value="{!$ObjectType.rC_Bios__Relationship__c.FieldSets.rC_Bios__ManageRelationships_CategoryOther}" rendered="{!AND(relationshipInsert.rC_Bios__Category__c == 'Other')}">
                            <apex:panelGrid columns="2" styleClass="industryTable" width="100%" columnClasses="labels,values">
                                <apex:outputText value="{!field.Label}" />
                                <apex:inputField value="{!relationshipInsert[field]}" />
                            </apex:panelGrid>
                        </apex:repeat>
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </apex:outputPanel>
                    <apex:outputPanel rendered="{!relationshipList.size > 0}" id="paging">
                    <apex:panelGrid columns="10">
                    <apex:actionRegion >
                        <apex:outputLabel value="Show" for="recordsPerPage"/>
                        <apex:selectList value="{!recordsPerPage}" size="1" id="recordsPerPage">
                            <apex:selectOptions value="{!pageSizeOptionList}"/>
                            <apex:actionSupport event="onchange" rerender="RelationshipTable,paging,NewRecord" action="{!initialize}" status="renderPageSizeStatus"/>
                        </apex:selectList>
                        <apex:actionStatus id="renderPageSizeStatus">
                            <apex:facet name="start">
                                <apex:image value="/img/loading.gif" style="vertical-align: middle; padding-left: 1em;" />
                            </apex:facet>
                        </apex:actionStatus>
                    </apex:actionRegion>
                    <apex:outputText value="{!showingRecord}"/>
                    <apex:outputPanel />
                    <apex:actionStatus id="nevigateStatus">
                        <apex:facet name="stop">
                            <apex:outputPanel >
                                <apex:commandLink action="{!firstPage}" value="First" rerender="RelationshipTable,paging,NewRecord" rendered="{!hasPrevious}" status="nevigateStatus"/>&nbsp;&nbsp;
                                <apex:commandLink action="{!previous}" value="Previous" rerender="RelationshipTable,paging,NewRecord" rendered="{!hasPrevious}" status="nevigateStatus"/>&nbsp;&nbsp;
                                <apex:commandLink action="{!next}" value="Next" rerender="RelationshipTable,paging,NewRecord" rendered="{!hasNext}" status="nevigateStatus"/>&nbsp;&nbsp;
                                <apex:commandLink action="{!lastPage}" value="Last" rerender="RelationshipTable,paging,NewRecord" rendered="{!hasNext}" status="nevigateStatus"/>&nbsp;&nbsp;
                            </apex:outputPanel>
                        </apex:facet>
                        <apex:facet name="start">
                            <apex:image value="/img/loading.gif" style="vertical-align: middle; padding-left: 1em;" />
                        </apex:facet>
                    </apex:actionStatus>
                </apex:panelGrid>
            </apex:outputPanel>
        </apex:pageBlock>
    </apex:form>
</apex:page>