/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Contact_RollupAddressesBatchable implements Database.Batchable<SObject> {
    global Contact_RollupAddressesBatchable() {

    }
    global void execute(Database.BatchableContext batchableContext, List<rC_Bios__Contact_Address__c> contactAddressList) {

    }
    global void finish(Database.BatchableContext batchableContext) {

    }
    global Database.QueryLocator start(Database.BatchableContext batchableContext) {
        return null;
    }
}
