/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ActionPlan_MassInsertBatchable implements Database.Batchable<SObject> {
    global ActionPlan_MassInsertBatchable(List<Id> idList, String sobjectType, rC_Bios__Action_Plan__c actionPlan, List<rC_Bios__Action_Plan_Task__c> actionPlanTaskList) {

    }
    global void execute(Database.BatchableContext batchableContext, List<SObject> sobjectList) {

    }
    global void finish(Database.BatchableContext batchableContext) {

    }
    global Database.QueryLocator start(Database.BatchableContext batchableContext) {
        return null;
    }
}
