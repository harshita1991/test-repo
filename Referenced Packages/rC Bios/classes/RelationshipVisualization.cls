/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RelationshipVisualization extends rC_Bios.SobjectExtension {
    @RemoteAction
    global static List<Campaign> getCampaignsMatching(String filter) {
        return null;
    }
    @RemoteAction
    global static rC_Bios.RelationshipVisualization.RelationshipSet getRelationshipSet(List<String> recordIds) {
        return null;
    }
    @RemoteAction
    global static List<CampaignMember> insertCampaignMembers(String campaignId, List<String> contactIds) {
        return null;
    }
global class Link {
    global String category;
    global String source;
    global String target;
    global Link() {

    }
}
global class Node {
    global List<rC_Bios.RelationshipVisualization.NodeField> fields;
    global String id;
    global String name;
    global String recordType;
    global String type;
    global Node() {

    }
}
global class NodeField {
    global String label;
    global String path;
    global String type;
    global String value;
    global NodeField() {

    }
}
global class RelationshipSet {
    global List<rC_Bios.RelationshipVisualization.Link> links;
    global List<rC_Bios.RelationshipVisualization.Node> nodes;
    global RelationshipSet() {

    }
}
}
