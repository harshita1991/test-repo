/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Settings {
    global static rC_Bios__Account_Address_Setting__c ACCOUNT_ADDRESS_SETTING;
    global static rC_Bios__Account_Address_Validation__c ACCOUNT_ADDRESS_VALIDATION;
    global static rC_Bios__Account_Setting__c ACCOUNT_SETTING;
    global static rC_Bios__Action_Plan_Setting__c ACTION_PLAN_SETTING;
    global static rC_Bios__Action_Plan_Task_Setting__c ACTION_PLAN_TASK_SETTING;
    global static rC_Bios__Address_Setting__c ADDRESS_SETTING;
    global static rC_Bios__Campaign_Setting__c CAMPAIGN_SETTING;
    global static rC_Bios__Contact_Address_Setting__c CONTACT_ADDRESS_SETTING;
    global static rC_Bios__Contact_Address_Validation__c CONTACT_ADDRESS_VALIDATION;
    global static rC_Bios__Contact_Setting__c CONTACT_SETTING;
    global static rC_Bios__Contact_Validation__c CONTACT_VALIDATION;
    global static Map<String,Schema.SObjectType> GLOBAL_SCHEMA;
    global static rC_Bios__Lead_Setting__c LEAD_SETTING;
    global static rC_Bios__Opportunity_Setting__c OPPORTUNITY_SETTING;
    global static rC_Bios__Preference_Setting__c PREFERENCE_SETTING;
    global static rC_Bios__Preference_Validation__c PREFERENCE_VALIDATION;
    global static rC_Bios__Relationship_Settings__c RELATIONSHIP_SETTING;
    global static rC_Bios__Relationship_Validation__c RELATIONSHIP_VALIDATION;
    global static rC_Bios__Salutation_Setting__c SALUTATION_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_ACCOUNT;
    global static Schema.DescribeSObjectResult SCHEMA_ACCOUNT_ADDRESS;
    global static Schema.DescribeSObjectResult SCHEMA_ACCOUNT_ADDRESS_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_ACCOUNT_RECORD_TYPE_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_ACCOUNT_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_ACTION_PLAN;
    global static Schema.DescribeSObjectResult SCHEMA_ACTION_PLAN_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_ACTION_PLAN_TASK;
    global static Schema.DescribeSObjectResult SCHEMA_ACTION_PLAN_TASK_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_ADDRESS;
    global static Schema.DescribeSObjectResult SCHEMA_ADDRESS_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_CAMPAIGN;
    global static Schema.DescribeSObjectResult SCHEMA_CAMPAIGN_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_CONTACT;
    global static Schema.DescribeSObjectResult SCHEMA_CONTACT_ADDRESS;
    global static Schema.DescribeSObjectResult SCHEMA_CONTACT_ADDRESS_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_CONTACT_RECORD_TYPE_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_CONTACT_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_LEAD;
    global static Schema.DescribeSObjectResult SCHEMA_LEAD_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_OPPORTUNITY;
    global static Schema.DescribeSObjectResult SCHEMA_OPPORTUNITY_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_PREFERENCE;
    global static Schema.DescribeSObjectResult SCHEMA_PREFERENCE_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_RELATIONSHIP;
    global static Schema.DescribeSObjectResult SCHEMA_RELATIONSHIP_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_SALUTATION;
    global static Schema.DescribeSObjectResult SCHEMA_SALUTATION_SETTING;
    global static Schema.DescribeSObjectResult SCHEMA_TASK_SETTING;
    global static rC_Bios__Task_Setting__c TASK_SETTING;
    global Settings() {

    }
}
