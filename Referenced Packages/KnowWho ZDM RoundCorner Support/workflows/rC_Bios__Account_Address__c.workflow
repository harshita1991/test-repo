<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Remove_RC_Address_Line_2_Values</fullName>
        <field>rC_Bios__Original_Street_Line_2__c</field>
        <name>Remove RC Address Line 2 Values</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_KnowWho_Address_Update</fullName>
        <field>KnowWho_Address_Update__c</field>
        <literalValue>0</literalValue>
        <name>Reset KnowWho Address Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Initial_Street_1_on_Change</fullName>
        <field>Initial_Street_1__c</field>
        <formula>PRIORVALUE(rC_Bios__Original_Street_Line_1__c)</formula>
        <name>Set Initial Street 1 on Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Initial_Street_2_on_Change</fullName>
        <description>Set Initial Street 2 on Change to RC Street 2 Field</description>
        <field>Initial_Street_2__c</field>
        <formula>PRIORVALUE(rC_Bios__Original_Street_Line_2__c)</formula>
        <name>Set Initial Street 2 on Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RC_City_to_USPS_City</fullName>
        <field>rC_Bios__Original_City__c</field>
        <formula>USPS_City__c</formula>
        <name>Set RC City to USPS City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RC_PostalCode_to_USPS_PostalCode</fullName>
        <field>rC_Bios__Original_Postal_Code__c</field>
        <formula>USPS_Zip__c</formula>
        <name>Set RC PostalCode to USPS PostalCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RC_State_to_USPS_State</fullName>
        <field>rC_Bios__Original_State__c</field>
        <formula>USPS_State__c</formula>
        <name>Set RC State to USPS State</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RC_Street_to_USPS_Street_1</fullName>
        <field>rC_Bios__Original_Street_Line_1__c</field>
        <formula>if (NOT(ISBLANK(USPS_Street_2__c)),

USPS_Street__c + &quot; &quot; + USPS_Street_2__c,
USPS_Street__c
)</formula>
        <name>Set RC Street to USPS Street 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Capture Initial Street 1 Address Value Change</fullName>
        <actions>
            <name>Set_Initial_Street_1_on_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If RC Street 1 changes, capture original value in Initial Street 1 for later review</description>
        <formula>ISCHANGED(rC_Bios__Original_Street_Line_1__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Capture Initial Street 2 Address Value Change</fullName>
        <actions>
            <name>Set_Initial_Street_2_on_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>If RC Street 2 changes, capture original value in Initial Street 2 for later review</description>
        <formula>ISCHANGED(rC_Bios__Original_Street_Line_2__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update RC Address when ZDM NOT Verified</fullName>
        <actions>
            <name>Remove_RC_Address_Line_2_Values</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_KnowWho_Address_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_RC_City_to_USPS_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_RC_PostalCode_to_USPS_PostalCode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_RC_State_to_USPS_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_RC_Street_to_USPS_Street_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This rule gives option to update RC address data even if ZDM Address not considered deliverable, but did match against zip code database</description>
        <formula>AND (   USPS_Verified__c &lt;&gt; TRUE,   OR (  ISCHANGED(USPS_City__c),  ISCHANGED(USPS_State__c),  ISCHANGED(USPS_Street__c),  ISCHANGED(USPS_Street_2__c),  ISCHANGED(USPS_Zip__c),  ISNEW()  ),   AND(  NOT(ISBLANK(USPS_Street__c)),  NOT(ISBLANK(USPS_City__c)),  NOT(ISBLANK(USPS_State__c)),  NOT(ISBLANK(USPS_Zip__c))   )   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update RC Address when ZDM Verified</fullName>
        <actions>
            <name>Remove_RC_Address_Line_2_Values</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reset_KnowWho_Address_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_RC_City_to_USPS_City</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_RC_PostalCode_to_USPS_PostalCode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_RC_State_to_USPS_State</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_RC_Street_to_USPS_Street_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>rC_Bios__Account_Address__c.USPS_Verified__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>rC_Bios__Account_Address__c.KnowWho_Address_Update__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When a cleaned address is returned from ZDM, set RC address fields = cleaned address</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
