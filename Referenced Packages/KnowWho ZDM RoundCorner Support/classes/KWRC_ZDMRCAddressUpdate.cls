/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class KWRC_ZDMRCAddressUpdate {
    global Boolean bSetVerifiedAddress {
        get;
        set;
    }
    global SObject rcAddressObject {
        get;
        set;
    }
    global kwzd.KWD_ExternalAccessWrapper zdmExternalWrapper {
        get;
        set;
    }
    global KWRC_ZDMRCAddressUpdate() {

    }
    @Deprecated
    global Boolean executeupdate() {
        return null;
    }
    global static void updateAccountContactZDMDetails(List<rC_Bios__Account_Address__c> accountaddrforupdate) {

    }
    @InvocableMethod(label='Update Related Accounts' description='Deprecated')
    global static void updateAccountZDMDetails(List<Id> aaids) {

    }
}
