<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Holds information related to a Venue at which an Event is offered.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Available_Services__c</fullName>
        <deprecated>false</deprecated>
        <description>Formula field returning the Service Information values which are true.</description>
        <externalId>false</externalId>
        <formula>IF(Has_Audio__c, &apos;Audio; &apos;, &apos;&apos;) + IF(Has_Video__c, &apos;Video; &apos;, &apos;&apos;) + IF(Has_Power__c, &apos;Power; &apos;, &apos;&apos;) + IF(Has_Internet__c, &apos;Internet; &apos;, &apos;&apos;) + IF(Has_Podium__c, &apos;Podium; &apos;, &apos;&apos;)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>Indicates services available.</inlineHelpText>
        <label>Available Services</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Capacity__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated, shows the maximum number of Attendees this venue will hold.</description>
        <externalId>false</externalId>
        <inlineHelpText>Maximum number of Attendees this venue will hold.</inlineHelpText>
        <label>Capacity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directions__c</fullName>
        <deprecated>false</deprecated>
        <description>Directions to the venue.</description>
        <externalId>false</externalId>
        <inlineHelpText>Directions on how to get to the venue.</inlineHelpText>
        <label>Directions</label>
        <length>32000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Format__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated, indicates the type of venue.</description>
        <externalId>false</externalId>
        <inlineHelpText>Type of venue.</inlineHelpText>
        <label>Format</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Conference Booth</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Conference Hall</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Parking</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Section/Row/Seat</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Table</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Ballroom</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>General_Rules__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated, indicates &quot;General Rules&quot; regarding the venue.</description>
        <externalId>false</externalId>
        <inlineHelpText>&quot;General Rules&quot; regarding the venue.</inlineHelpText>
        <label>General Rules</label>
        <length>10000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>10</visibleLines>
    </fields>
    <fields>
        <fullName>Has_Audio__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Manually populated, indicates the venue provides audio support.</description>
        <externalId>false</externalId>
        <inlineHelpText>Venue provides audio support.</inlineHelpText>
        <label>Has Audio?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Has_Internet__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Manually populated, indicates the venue provides internet.</description>
        <externalId>false</externalId>
        <inlineHelpText>Venue provides internet.</inlineHelpText>
        <label>Has Internet?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Has_Podium__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Manually populated, indicates the venue provides a speaking podium.</description>
        <externalId>false</externalId>
        <inlineHelpText>Venue provides a speaking podium.</inlineHelpText>
        <label>Has Podium?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Has_Power__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Manually populated, indicates the venue provides AC power.</description>
        <externalId>false</externalId>
        <inlineHelpText>Venue provides AC power.</inlineHelpText>
        <label>Has Power?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Has_Video__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Manually populated, indicates the venue provides video support.</description>
        <externalId>false</externalId>
        <inlineHelpText>Venue provides video support.</inlineHelpText>
        <label>Has Video?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Layout__c</fullName>
        <deprecated>false</deprecated>
        <description>Conference Style - Rectangle tables arranged in a open, closed or &quot;u&quot; shaped rectangle.	
Round Style - Round tables seating 4-8.
Audience Style - Chairs facing one direction - no tables.
Square Style - Two rectangles combined to create a square seating up to 10.
Classroom Style - Rectangle tables in rows facing one direction. Seating on one side only	
Other - Any combination of round or rectangle tables / chairs.</description>
        <externalId>false</externalId>
        <inlineHelpText>Conference Style-Open, closed or u shaped rectangle. Round Style-Round tables seating 8.  Audience Style-Chairs, one direction, no tables.  Square Style-Square seating 10.  Classroom Style-Rectangle tables, rows, one direction. Other- Any other style.</inlineHelpText>
        <label>Layout</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Audience</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Booth</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Classroom</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Conference</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Round</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Square</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Other</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Location__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated, provides the description of the Venue &quot;Location.&quot;</description>
        <externalId>false</externalId>
        <inlineHelpText>Description of the Venue &quot;Location.&quot;</inlineHelpText>
        <label>Location</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Parent__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Manually populated, Indicates the &quot;Parent Venue&quot; over this Venue.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the &quot;Parent Venue&quot; over this Venue.</inlineHelpText>
        <label>Parent Venue</label>
        <referenceTo>Venue__c</referenceTo>
        <relationshipLabel>Sub-Venues</relationshipLabel>
        <relationshipName>Venues</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Parking__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated, describes the parking arrangements for this Venue.</description>
        <externalId>false</externalId>
        <inlineHelpText>Describes the parking arrangements for this Venue.</inlineHelpText>
        <label>Parking</label>
        <length>1000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Public_Transportation__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated, describes the &quot;Public Transportation&quot; available at or near this venue.</description>
        <externalId>false</externalId>
        <inlineHelpText>Describes the &quot;Public Transportation&quot; available at or near this venue.</inlineHelpText>
        <label>Public Transportation</label>
        <length>1000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Registered_Limit__c</fullName>
        <deprecated>false</deprecated>
        <description>Auto populated, indicates the maximum number of registrants allowed at this Venue.</description>
        <externalId>false</externalId>
        <inlineHelpText>Maximum number of registrants allowed at this Venue.</inlineHelpText>
        <label>Venue Capacity</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Ticket_Office_Notes__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated, provides comments related to the ticket office.</description>
        <externalId>false</externalId>
        <inlineHelpText>Comments related to the ticket office.</inlineHelpText>
        <label>Ticket Office Notes</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Ticket_Office_Phone__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated, provides the phone number to the ticket office.</description>
        <externalId>false</externalId>
        <inlineHelpText>Phone number to the ticket office.</inlineHelpText>
        <label>Ticket Office Phone</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Venue_City__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated, City where the venue is located.</description>
        <externalId>false</externalId>
        <inlineHelpText>City where the venue is located.</inlineHelpText>
        <label>Venue City</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Venue_Country__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated, Country where the venue is located.</description>
        <externalId>false</externalId>
        <inlineHelpText>Country where the venue is located.</inlineHelpText>
        <label>Venue Country</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Venue_Phone__c</fullName>
        <deprecated>false</deprecated>
        <description>Venue phone number.</description>
        <externalId>false</externalId>
        <inlineHelpText>Venue phone number.</inlineHelpText>
        <label>Venue Phone</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Phone</type>
    </fields>
    <fields>
        <fullName>Venue_Postal_Code__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated, Postal Code where the venue is located.</description>
        <externalId>false</externalId>
        <inlineHelpText>Postal Code where the venue is located.</inlineHelpText>
        <label>Venue Postal Code</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Venue_State__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated, State/Province where the venue is located.</description>
        <externalId>false</externalId>
        <inlineHelpText>State/Province where the venue is located.</inlineHelpText>
        <label>Venue State/Province</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Venue_Street__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated, Street where the venue is located.</description>
        <externalId>false</externalId>
        <inlineHelpText>Street where the venue is located.</inlineHelpText>
        <label>Venue Street</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Waitlisted_Limit__c</fullName>
        <deprecated>false</deprecated>
        <description>Auto populated, indicates the maximum number of waitlisted attendees allowed at this venue.</description>
        <externalId>false</externalId>
        <inlineHelpText>Maximum number of waitlisted attendees allowed at this Venue.</inlineHelpText>
        <label>Waitlist Limit</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Venue</label>
    <nameField>
        <label>Venue Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Venues</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Format__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Layout__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Capacity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Available_Services__c</customTabListAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
</CustomObject>
