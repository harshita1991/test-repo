<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Event_As_Error</fullName>
        <description>This field update make Event State as Error</description>
        <field>Event_State__c</field>
        <literalValue>Error</literalValue>
        <name>Update Event As Error</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>Event__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Update Event As Error</fullName>
        <actions>
            <name>Update_Event_As_Error</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Queue__c.Queue_State__c</field>
            <operation>equals</operation>
            <value>Error</value>
        </criteriaItems>
        <description>This workflow rule triggers when queue record gets updated.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
