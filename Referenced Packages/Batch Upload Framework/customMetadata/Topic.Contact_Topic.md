<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contact Topic</label>
    <protected>false</protected>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">This topic is related to contact matching/processing for each Batch Upload Record</value>
    </values>
    <values>
        <field>IsMatching__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Is_Last_Topic__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Parent_Topics__c</field>
        <value xsi:type="xsd:string">Account_Topic</value>
    </values>
    <values>
        <field>Sequence__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Topic_ID__c</field>
        <value xsi:type="xsd:string">ContactTopic</value>
    </values>
</CustomMetadata>
