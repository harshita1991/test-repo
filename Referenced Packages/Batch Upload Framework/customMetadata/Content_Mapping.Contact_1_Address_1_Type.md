<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contact 1 Address 1 Type</label>
    <protected>false</protected>
    <values>
        <field>Id__c</field>
        <value xsi:type="xsd:string">Contact1Address1TypeMap</value>
    </values>
    <values>
        <field>IsExternalID__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Process__c</field>
        <value xsi:type="xsd:string">Contact_1_Address_1_Process</value>
    </values>
    <values>
        <field>Source_Field__c</field>
        <value xsi:type="xsd:string">rC_Connect__Address_Type__c</value>
    </values>
    <values>
        <field>Source_SObject__c</field>
        <value xsi:type="xsd:string">rC_Connect__Batch_Upload__c</value>
    </values>
    <values>
        <field>Target_Field__c</field>
        <value xsi:type="xsd:string">{&quot;FIELDNAME&quot;:&quot;rC_Bios__Type__c&quot;,&quot;DATATYPE&quot;:&quot;PICKLIST&quot;}</value>
    </values>
    <values>
        <field>Target_SObject__c</field>
        <value xsi:type="xsd:string">rC_Bios__Contact_Address__c</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Insert &amp; Update</value>
    </values>
</CustomMetadata>
