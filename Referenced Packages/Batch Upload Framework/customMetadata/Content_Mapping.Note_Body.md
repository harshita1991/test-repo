<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Note Body</label>
    <protected>false</protected>
    <values>
        <field>Id__c</field>
        <value xsi:type="xsd:string">NoteCommentMap</value>
    </values>
    <values>
        <field>IsExternalID__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Process__c</field>
        <value xsi:type="xsd:string">Note_Process</value>
    </values>
    <values>
        <field>Source_Field__c</field>
        <value xsi:type="xsd:string">rC_Connect__Notes_Comments__c</value>
    </values>
    <values>
        <field>Source_SObject__c</field>
        <value xsi:type="xsd:string">rC_Connect__Batch_Upload__c</value>
    </values>
    <values>
        <field>Target_Field__c</field>
        <value xsi:type="xsd:string">{&quot;fieldName&quot;:&quot;Body&quot;,&quot;dataType&quot;:&quot;textArea&quot;}</value>
    </values>
    <values>
        <field>Target_SObject__c</field>
        <value xsi:type="xsd:string">Note</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Insert</value>
    </values>
</CustomMetadata>
