<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contact 1 Address 1 Process</label>
    <protected>false</protected>
    <values>
        <field>Batch_Class_Name__c</field>
        <value xsi:type="xsd:string">BUP_ContactAddressProcessBatchable</value>
    </values>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:type="xsd:double">200.0</value>
    </values>
    <values>
        <field>Commit_Service__c</field>
        <value xsi:type="xsd:string">BUP_ContactAddressCommitService</value>
    </values>
    <values>
        <field>Field_Mappings__c</field>
        <value xsi:type="xsd:string">{&quot;Id&quot;:&quot;Contact_1_Address_1_Matched__c&quot;,&quot;Status&quot;:&quot;Contact_1_Address_1_Status__c&quot;}</value>
    </values>
    <values>
        <field>Id__c</field>
        <value xsi:type="xsd:string">ContactAddressProcess</value>
    </values>
    <values>
        <field>Matching_Service__c</field>
        <value xsi:type="xsd:string">BUP_ContactAddressMatchingService</value>
    </values>
    <values>
        <field>Process_Sequence__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Record_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Retry_Attempt__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Retry_Interval__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SObject__c</field>
        <value xsi:type="xsd:string">rC_Bios__Contact_Address__c</value>
    </values>
    <values>
        <field>Topic__c</field>
        <value xsi:type="xsd:string">Contact_Address_Topic</value>
    </values>
    <values>
        <field>isMatching__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
