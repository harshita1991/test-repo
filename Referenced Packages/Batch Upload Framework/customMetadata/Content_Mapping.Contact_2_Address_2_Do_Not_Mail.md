<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Contact 2 Address 2 Do Not Mail</label>
    <protected>false</protected>
    <values>
        <field>Id__c</field>
        <value xsi:type="xsd:string">Contact2Address2DoNotMail</value>
    </values>
    <values>
        <field>IsExternalID__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Process__c</field>
        <value xsi:type="xsd:string">Contact_2_Address_2_Process</value>
    </values>
    <values>
        <field>Source_Field__c</field>
        <value xsi:type="xsd:string">rC_Connect__Address_2_Do_Not_Mail__c</value>
    </values>
    <values>
        <field>Source_SObject__c</field>
        <value xsi:type="xsd:string">rC_Connect__Batch_Upload__c</value>
    </values>
    <values>
        <field>Target_Field__c</field>
        <value xsi:type="xsd:string">{&quot;FIELDNAME&quot;:&quot;rC_Bios__Do_Not_Mail__c &quot;,&quot;DATATYPE&quot;:&quot;Boolean&quot;}</value>
    </values>
    <values>
        <field>Target_SObject__c</field>
        <value xsi:type="xsd:string">rC_Bios__Contact_Address__c</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
