<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Account Address 1 Process</label>
    <protected>false</protected>
    <values>
        <field>Batch_Class_Name__c</field>
        <value xsi:type="xsd:string">BUP_AccountAddressProcessBatchable</value>
    </values>
    <values>
        <field>Batch_Size__c</field>
        <value xsi:type="xsd:double">200.0</value>
    </values>
    <values>
        <field>Commit_Service__c</field>
        <value xsi:type="xsd:string">BUP_AccountAddressCommitService</value>
    </values>
    <values>
        <field>Field_Mappings__c</field>
        <value xsi:type="xsd:string">{&quot;Id&quot;:&quot;Account_Address_1_Matched__c&quot;,&quot;Status&quot;:&quot;Account_Address_Status__c&quot;}</value>
    </values>
    <values>
        <field>Id__c</field>
        <value xsi:type="xsd:string">AccountAddressProcess</value>
    </values>
    <values>
        <field>Matching_Service__c</field>
        <value xsi:type="xsd:string">BUP_AccountAddressMatchingService</value>
    </values>
    <values>
        <field>Process_Sequence__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Record_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Retry_Attempt__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Retry_Interval__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SObject__c</field>
        <value xsi:type="xsd:string">rC_Bios__Account_Address__c</value>
    </values>
    <values>
        <field>Topic__c</field>
        <value xsi:type="xsd:string">Account_Address_Topic</value>
    </values>
    <values>
        <field>isMatching__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
