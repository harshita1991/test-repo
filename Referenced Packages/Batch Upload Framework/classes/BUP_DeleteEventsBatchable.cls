/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class BUP_DeleteEventsBatchable implements Database.Batchable<SObject> {
    global BUP_DeleteEventsBatchable() {

    }
    global void execute(Database.BatchableContext bc, List<rC_BUP__Event__c> eventList) {

    }
    global void finish(Database.BatchableContext bc) {

    }
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return null;
    }
}
