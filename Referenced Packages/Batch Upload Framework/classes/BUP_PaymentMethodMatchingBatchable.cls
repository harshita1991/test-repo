/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class BUP_PaymentMethodMatchingBatchable extends rC_BUP.BUF_CustomMatchingService implements Database.Batchable<SObject>, Database.Stateful {
    global BUP_PaymentMethodMatchingBatchable() {

    }
    global BUP_PaymentMethodMatchingBatchable(Integer currentProcessSequence) {

    }
    global void execute(Database.BatchableContext BC, List<rC_BUP__Queue__c> matchingQueueList) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return null;
    }
}
