/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class BUF_NewQueueSchedulable implements System.Schedulable {
    global BUF_NewQueueSchedulable() {

    }
    global void execute(System.SchedulableContext SC) {

    }
}
