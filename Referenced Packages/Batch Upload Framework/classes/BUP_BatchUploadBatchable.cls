/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class BUP_BatchUploadBatchable extends rC_BUP.BUP_ProcessService implements Database.Batchable<SObject>, Database.Stateful {
    global void execute(Database.BatchableContext BC, List<rC_Connect__Batch_Upload__c> batchUploadScopeList) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return null;
    }
}
