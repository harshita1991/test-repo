<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>Hierarchy</customSettingsType>
    <description>These are settings to manage/control Batch Upload Framework processing.</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>BUF_CompletedEventScheduler_Scope__c</fullName>
        <deprecated>false</deprecated>
        <description>This field holds scope for BUF_CompletedEventScheduler. BUF_CompletedEventScheduler will retrieve total number of records mentioned in this field.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field holds scope for BUF_CompletedEventScheduler. BUF_CompletedEventScheduler will retrieve total number of records mentioned in this field.</inlineHelpText>
        <label>BUF_CompletedEventScheduler Scope</label>
        <precision>5</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BUF_CompletedQueueScheduler_Scope__c</fullName>
        <deprecated>false</deprecated>
        <description>This field holds scope for BUF_CompletedQueueScheduler. BUF_CompletedQueueScheduler will retrieve total number of records mentioned in this field.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field holds scope for BUF_CompletedQueueScheduler. BUF_CompletedQueueScheduler will retrieve total number of records mentioned in this field.</inlineHelpText>
        <label>BUF_CompletedQueueScheduler Scope</label>
        <precision>5</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BUF_ErrorEventScheduler_Scope__c</fullName>
        <deprecated>false</deprecated>
        <description>This field holds scope for BUF_ErrorEventScheduler. BUF_ErrorEventScheduler will retrieve total number of records mentioned in this field.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field holds scope for BUF_ErrorEventScheduler. BUF_ErrorEventScheduler will retrieve total number of records mentioned in this field.</inlineHelpText>
        <label>BUF_ErrorEventScheduler Scope</label>
        <precision>5</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BUF_NewEventScheduler_Scope__c</fullName>
        <deprecated>false</deprecated>
        <description>This field holds scope for BUF_NewEventScheduler. BUF_NewEventScheduler will retrieve total number of records mentioned in this field.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field holds scope for BUF_NewEventScheduler. BUF_NewEventScheduler will retrieve total number of records mentioned in this field.</inlineHelpText>
        <label>BUF_NewEventScheduler Scope</label>
        <precision>5</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BUF_NewQueueScheduler_Scope__c</fullName>
        <deprecated>false</deprecated>
        <description>This field holds scope for BUF_NewQueueScheduler. BUF_NewQueueScheduler will retrieve total number of records mentioned in this field.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field holds scope for BUF_NewQueueScheduler. BUF_NewQueueScheduler will retrieve total number of records mentioned in this field.</inlineHelpText>
        <label>BUF_NewQueueScheduler Scope</label>
        <precision>5</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Enable_Delete_Events_and_Queues__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>This setting, if set to true, enables BUP_DeleteEventsBatchable class to start execution to delete events/queues related to Committed Batch Upload Records once Batch Upload Framework completes processing of Batch Upload Records.</description>
        <externalId>false</externalId>
        <inlineHelpText>This setting, if set to true, enables BUP_DeleteEventsBatchable class to start execution to delete events/queues related to Committed Batch Upload Records once Batch Upload Framework completes processing of Batch Upload Records.</inlineHelpText>
        <label>Enable Delete Events and Queues</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Framework_Scheduler_Timeout_In_Minutes__c</fullName>
        <deprecated>false</deprecated>
        <description>This field holds Queued time for each framework scheduler, if any scheduler is in Queued/Holding state for more than mentioned time then Monitor Scheduler should abort the Queued Scheduler and should reschedule that particular scheduler again.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field holds Queued time for each framework scheduler, if any scheduler is in Queued/Holding state for more than mentioned time then Monitor Scheduler should abort the Queued Scheduler and should reschedule that particular scheduler again.</inlineHelpText>
        <label>Framework Scheduler Timeout (In Minutes)</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>In_Progress_Queue_Timeout_In_Min__c</fullName>
        <deprecated>false</deprecated>
        <description>This field specified timeout for Queues which stays in &quot;IN PROGRESS&quot; state for mentioned time, if so then BUF_AuditFailureSchedulable job will Flag such Queue, Events and Batch Upload Records.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field specified timeout for Queues which stays in &quot;IN PROGRESS&quot; state for mentioned time, if so then BUF_AuditFailureSchedulable job will Flag such Queue, Events and Batch Upload Records.</inlineHelpText>
        <label>In Progress Queue Timeout (In Min)</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Monitor_Scheduler_Interval_Time_Second__c</fullName>
        <deprecated>false</deprecated>
        <description>This field holds time value In Seconds which will be interval time between two successive processing of Monitor Schedulers.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field holds time value In Seconds which will be interval time between two successive processing of Monitor Schedulers.</inlineHelpText>
        <label>Monitor Scheduler Interval Time (In Sec)</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Run_Monitor_Scheduler_After_Matching__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, Monitor scheduler will auto triggered post matching completion else it will not schedule the Monitor scheduler.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, Monitor scheduler will auto triggered post matching completion else it will not schedule the Monitor scheduler.</inlineHelpText>
        <label>Run Monitor Scheduler After Matching</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>System_Failure_Scheduler_Time_In_Hrs__c</fullName>
        <deprecated>false</deprecated>
        <description>This field specified Scheduled job interval time for BUF_AuditFailureSchedulable, which captures system failure issues and updates details on related Queue, Event and Batch Upload Records.</description>
        <externalId>false</externalId>
        <inlineHelpText>This field specified Scheduled job interval time for BUF_AuditFailureSchedulable, which captures system failure issues and updates details on related Queue, Event and Batch Upload Records.</inlineHelpText>
        <label>System Failure Scheduler Time (In Hrs)</label>
        <precision>18</precision>
        <required>true</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Batch Upload Framework Settings</label>
    <visibility>Public</visibility>
</CustomObject>
