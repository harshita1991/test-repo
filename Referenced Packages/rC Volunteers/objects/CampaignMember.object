<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <fullName>Assigned_Shift_Date__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Assigned Shift Date</label>
        <required>false</required>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Feedback__c</fullName>
        <deprecated>false</deprecated>
        <description>Provide feedback on the Volunteer.</description>
        <externalId>false</externalId>
        <inlineHelpText>Provide feedback on the Volunteer.</inlineHelpText>
        <label>Feedback</label>
        <required>false</required>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Group_Name__c</fullName>
        <deprecated>false</deprecated>
        <description>Specify the Group Name.</description>
        <externalId>false</externalId>
        <inlineHelpText>Specify the Group Name.</inlineHelpText>
        <label>Group Name</label>
        <length>55</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Group_Signup__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Specify if it is a Group signup.</description>
        <externalId>false</externalId>
        <inlineHelpText>Specify if it is a Group signup.</inlineHelpText>
        <label>Group Signup?</label>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Members_In_Group__c</fullName>
        <deprecated>false</deprecated>
        <description>Specify the members in the Group for Volunteering.</description>
        <externalId>false</externalId>
        <inlineHelpText>Specify the members in the Group for Volunteering.</inlineHelpText>
        <label># Members In Group</label>
        <precision>5</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Shift_End_Time__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Shift End Time</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Shift_Start_Time__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Shift Start Time</label>
        <length>50</length>
        <required>false</required>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Shift_Status__c</fullName>
        <deprecated>false</deprecated>
        <description>Specify the Shift Status of the Volunteering Member.</description>
        <externalId>false</externalId>
        <inlineHelpText>Specify the Shift Status of the Volunteering Member.</inlineHelpText>
        <label>Volunteer Status</label>
        <required>false</required>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Assigned</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Canceled</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Waitlisted</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Completed</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Volunteer_Count__c</fullName>
        <defaultValue>1</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates the number of times the Volunteer has attended Volunteering.This value rolls up to the Contact.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the number of times the Volunteer has attended Volunteering.This value rolls up to the Contact.</inlineHelpText>
        <label>Volunteer Count</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Volunteer_Hours__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <description>Indicates the number of hours the Volunteer has Volunteered.This value rolls up to the Contact.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates the number of hours the Volunteer has Volunteered.This value rolls up to the Contact.</inlineHelpText>
        <label>Volunteer Hours</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Volunteer_Karma__c</fullName>
        <defaultValue>10</defaultValue>
        <deprecated>false</deprecated>
        <description>Provide credits to Volunteer for the Volunteering Karma.This value rolls up to the Contact.</description>
        <externalId>false</externalId>
        <inlineHelpText>Provide credits to Volunteer  for the Volunteering Karma.This value rolls up to the Contact.</inlineHelpText>
        <label>Volunteer Karma</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <recordTypes>
        <fullName>Scheduled_Volunteer</fullName>
        <active>true</active>
        <label>Volunteer Member</label>
        <picklistValues>
            <picklist>Shift_Status__c</picklist>
            <values>
                <fullName>Assigned</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Canceled</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Completed</fullName>
                <default>false</default>
            </values>
            <values>
                <fullName>Waitlisted</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <validationRules>
        <fullName>Shift_Status_Required</fullName>
        <active>true</active>
        <description>Shift Status value is required to save a record.</description>
        <errorConditionFormula>AND($Setup.CampaignMember_Validation__c.Shift_Status_Required__c, Campaign.RecordType.DeveloperName = &apos;Job Shifts&apos;,
ISPICKVAL(Shift_Status__c,&apos;&apos;) )</errorConditionFormula>
        <errorDisplayField>Members_In_Group__c</errorDisplayField>
        <errorMessage>Required field: Shift Status</errorMessage>
    </validationRules>
</CustomObject>
