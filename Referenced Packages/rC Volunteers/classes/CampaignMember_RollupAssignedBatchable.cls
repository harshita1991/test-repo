/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CampaignMember_RollupAssignedBatchable implements Database.Batchable<SObject> {
    global CampaignMember_RollupAssignedBatchable() {

    }
    global void execute(Database.BatchableContext batchableContext, List<CampaignMember> campaignMemberList) {

    }
    global void finish(Database.BatchableContext batchableContext) {

    }
    global Database.QueryLocator start(Database.BatchableContext batchableContext) {
        return null;
    }
}
