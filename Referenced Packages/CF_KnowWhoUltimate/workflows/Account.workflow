<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_KW_Account_Exec_Former_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>KW_Executive_Former</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set KW Account Exec Former Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_KW_Account_Executive_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>KW_Executive</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set KW Account Executive Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set KW Account Executive Former Record Type</fullName>
        <actions>
            <name>Set_KW_Account_Exec_Former_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.kw__KW_Template__c</field>
            <operation>equals</operation>
            <value>AccountExecutiveFormer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set KW Account Executive Record Type</fullName>
        <actions>
            <name>Set_KW_Account_Executive_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.kw__KW_Template__c</field>
            <operation>equals</operation>
            <value>AccountExecutive</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
