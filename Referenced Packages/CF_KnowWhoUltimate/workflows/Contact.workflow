<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_KW_Contact_Exec_ElecRecord_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>KW_Executive_Elected_Record</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set KW Contact Exec ElecRecord Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_KW_Contact_Exec_Elec_Fmr_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>KW_Executive_Elected_Former</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set KW Contact Exec Elec Fmr Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_KW_Contact_Exec_Fmr_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>KW_Executive_Former</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set KW Contact Exec Fmr Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_KW_Contact_Exec_Staffer_Fmr_R_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>KW_Executive_Staffer_Former</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set KW Contact Exec Staffer Fmr R Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_KW_Contact_Exec_Staffer_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>KW_Executive_Staffer_Former</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set KW Contact Exec Staffer Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_KW_Contact_Executive_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>KW_Executive_Record</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set KW Contact Executive Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set KW Contact Exec Elec Fmr Record Type</fullName>
        <actions>
            <name>Set_KW_Contact_Exec_Elec_Fmr_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.kw__KW_Template__c</field>
            <operation>equals</operation>
            <value>ContactExecutiveElectedFormer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set KW Contact Exec Fmr Record Type</fullName>
        <actions>
            <name>Set_KW_Contact_Exec_Fmr_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.kw__KW_Template__c</field>
            <operation>equals</operation>
            <value>ContactExecutiveFormer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set KW Contact Exec Staffer Fmr Record Type</fullName>
        <actions>
            <name>Set_KW_Contact_Exec_Staffer_Fmr_R_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.kw__KW_Template__c</field>
            <operation>equals</operation>
            <value>ContactExecutiveStafferFormer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set KW Contact Exec Staffer Record Type</fullName>
        <actions>
            <name>Set_KW_Contact_Exec_Staffer_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.kw__KW_Template__c</field>
            <operation>equals</operation>
            <value>ContactExecutiveStaffer</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set KW Contact Executive Elected Record Type</fullName>
        <actions>
            <name>Set_KW_Contact_Exec_ElecRecord_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.kw__KW_Template__c</field>
            <operation>equals</operation>
            <value>ContactExecutiveElected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set KW Contact Executive Record Type</fullName>
        <actions>
            <name>Set_KW_Contact_Executive_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.kw__KW_Template__c</field>
            <operation>equals</operation>
            <value>ContactExecutive</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
