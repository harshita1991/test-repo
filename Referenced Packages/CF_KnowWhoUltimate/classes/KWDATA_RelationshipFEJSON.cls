/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class KWDATA_RelationshipFEJSON {
    global List<kw_cuwfe.KWDATA_RelationshipFEJSON.Data> Data;
    global KWDATA_RelationshipFEJSON() {

    }
    global static kw_cuwfe.KWDATA_RelationshipFEJSON parse(String json) {
        return null;
    }
global class Data {
    global String AccountID;
    global String AccountSortOrder;
    global String ContactID;
    global String ContactSortOrder;
    global String EnteredBy;
    global String KnowWhoID;
    global String LastEntered;
    global String ParentID;
    global String PersonID;
    global String PrimaryID;
    global String RecordID;
    global String RoleDesc;
    global Data() {

    }
}
}
