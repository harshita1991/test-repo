/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class KWDATA_ProcessDataBatchExecutive extends kw.KWDATA_ProcessDataBatchBaseClass {
    global KWDATA_ProcessDataBatchExecutive() {

    }
    global override void processRelationships(String sDirectoryType, String skwObjectType, String sStagingId) {

    }
}
