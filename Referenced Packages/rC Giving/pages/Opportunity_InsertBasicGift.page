<apex:page standardController="Opportunity" extensions="rC_Giving.Opportunity_InsertBasicGift" action="{!initializeSafe}" >
    <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
    <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>

    <apex:sectionHeader title="New {!$ObjectType.Opportunity.Label}" subtitle="{!BLANKVALUE(opportunity.Name, 'Quick Create')}"></apex:sectionHeader>

    <style type="text/css">
    .giving_wizard [type="myoutercontainer"] th.labelCol {
        vertical-align: middle !important;
    }

    .giving_wizard [type="payment-section"] {
        background-color: #eee;
        border: 1px solid gray;
        padding-top: 1%;
        padding-bottom: 2%;
    }

    .giving_wizard [type="payment-section-header"] label {
        margin-left: 0.25em;
    }

    .giving_wizard input[type="text"] {
        padding: 0.10em;
    }

    .giving_wizard select.auto_width {
        padding: 0.2em;
        min-width: 0em;
    }

    .giving_wizard select {
        padding: 0em;
        min-width: 10em;
        margin-left: 0;
    }
    </style>
    <apex:pageMessages />

    <apex:form id="giving_wizard" styleClass="giving_wizard" >
        <apex:pageBlock title="Basic Gift Data" mode="edit">
            <apex:pageBlockButtons >
                <apex:commandButton value="Save" action="{!upsertOpportunity}" onclick="return isNotEmpty()"/>
                <apex:commandButton value="Cancel" action="{!Cancel}" />
                <apex:commandButton id="btnOpenDialog" styleClass="switch-to-adv-btn" value="Switch to Advanced Edit" action="{!getRedirectToOppManage}"></apex:commandButton>
            </apex:pageBlockButtons>

            <apex:pageBlockSection columns="1" collapsible="false" showHeader="false">
                <!--***AMOUNT*** required="true"-->
                <apex:pageBlockSectionItem html-type="myoutercontainer">
                    <apex:outputLabel for="date" value="{!$ObjectType.Opportunity.Fields.rC_Giving__Giving_Amount__c.Label}"></apex:outputLabel>
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel layout="block" styleClass="requiredBlock"></apex:outputPanel>
                        <apex:inputField onkeypress="return isNumberKeyGivingAmount(event, id)" id="amount" value="{!opportunity.rC_Giving__Giving_Amount__c}"  style="width:150px; margin-left:0px; padding:0em; font-size:12px;"></apex:inputField>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>

                <!-- ***Primary Contact*** -->
              <apex:pageBlockSectionItem rendered="{!AND(isGiving==true,AND(isHigherEdGivingRecordType))}">
                
                    <apex:outputLabel value="{!$ObjectType.Opportunity.Fields.rC_Giving__Primary_Contact__c.Label}" />
                    <apex:outputPanel layout="none">
                        <apex:outputPanel layout="none" rendered="{!ISBLANK(opportunity.Id)}">
                           
                           <apex:actionRegion >
                                 <apex:outputPanel layout="block" >    
                                      
                                   <apex:inputField value="{!opportunity.rC_Giving__Primary_Contact__c}" Id="contactId" styleClass="contactId" >
                                        <apex:actionSupport event="onchange" 
                                                            action="{!populateAccount}" 
                                                            rerender="myoutercontainer" 
                                                            status="details_status" />
                                    </apex:inputField>
                                
                              </apex:outputPanel>  
                                
                            </apex:actionRegion>
                        </apex:outputPanel>

                        <apex:outputPanel layout="none" rendered="{!NOT(ISBLANK(opportunity.Id))}">
                            <apex:outputField value="{!opportunity.rC_Giving__Primary_Contact__c}"/>
                        </apex:outputPanel>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>

                <!--***ACCOUNT*** required="true"-->
                <apex:pageBlockSectionItem id="myoutercontainer" html-type="myoutercontainer">
                    <apex:outputLabel for="accountName" value="{!$ObjectType.Account.Fields.Name.Label}"></apex:outputLabel>
                    <apex:outputPanel >
                    <apex:outputPanel layout="block" styleClass="requiredInput" rendered="{! AND(ISBLANK(opportunity.Id),NOT(isHigherEdGivingRecordType)) }" >
                        <apex:outputPanel layout="block" styleClass="requiredBlock"></apex:outputPanel>
                        <apex:actionRegion id="accountNameChange">
                            <apex:inputField id="accountName" value="{!opportunity.AccountId}" style="margin-left:0px; padding:0em; font-size:12px;" >
                                <apex:actionSupport event="onchange" status="counterStatus" reRender="paymentMethod" action="{!showDefaultPaymentMethod}" />
                                <apex:actionStatus id="counterStatus" startText=" (Please Wait...)" stopText=""></apex:actionStatus>
                            </apex:inputField>
                        </apex:actionRegion>
                    </apex:outputPanel>
                    <apex:outputPanel layout="none" rendered="{! OR( NOT(ISBLANK(opportunity.Id)),isHigherEdGivingRecordType) }">
                        <apex:outputField value="{!opportunity.AccountId}"/>
                    </apex:outputPanel>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>

                <!--***DATE*** required="true"-->
                <apex:pageBlockSectionItem html-type="myoutercontainer">
                    <apex:outputLabel for="date" value="{!$ObjectType.Opportunity.Fields.CloseDate.Label}"></apex:outputLabel>
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel layout="block" styleClass="requiredBlock"></apex:outputPanel>
                        <apex:inputField id="date" value="{!opportunity.CloseDate}" style="margin-left:0px; padding:0em; font-size:12px;" required="false"></apex:inputField>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>

                <!--***CPRT** required="true"-->
                <apex:pageBlockSectionItem html-type="myoutercontainer">
                    <apex:outputPanel >
                        <apex:outputLabel for="Campaign" value="Appeal" rendered="{!AND(isHigherEdGivingRecordType)}" />
                        <apex:outputLabel for="Campaign" value="Primary Campaign Source" rendered="{!NOT(isHigherEdGivingRecordType)}" ></apex:outputLabel>
                    </apex:outputPanel>
                    <apex:outputPanel layout="block" styleClass="requiredInput">
                        <apex:outputPanel layout="block" styleClass="requiredBlock"></apex:outputPanel>
                        <apex:inputField id="campaign" value="{!opportunity.CampaignId}" style="margin-left:0px; padding:0em; font-size:12px;"></apex:inputField>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>

                <!--***FREQUENCY***-->
                <apex:pageBlockSectionItem html-type="myoutercontainer">
                    <apex:outputLabel for="frequency" value="Frequency"></apex:outputLabel>
                    <apex:selectList id="frequency" value="{!opportunity.Giving_Frequency__c }" size="1" style="margin-left:0px; padding:0em; font-size:12px;" >
                        <apex:selectOptions Value="{!givingFrequencyItems}"></apex:selectOptions>
                    </apex:selectList>
                  </apex:pageBlockSectionItem>

                <!--  Sustainer -->
                <apex:pageBlockSectionItem html-type="myoutercontainer">
                    <apex:outputLabel for="sustainer" value="Sustainer"></apex:outputLabel>
                    <apex:inputField value="{!Opportunity.rC_Giving__Is_Sustainer__c}" id="Sustainer" style="margin-left:0px; padding:0em; font-size:12px;" />
                </apex:pageBlockSectionItem>

                <!--***Payment End DATE***-->
                <apex:pageBlockSectionItem html-type="myoutercontainer">
                    <apex:outputLabel for="paymentEndDate" value="Payment End Date"></apex:outputLabel>
                    <apex:inputField id="paymentEndDate" value="{!Opportunity.rC_Giving__Payment_End_Date__c}" style="margin-left:0px; padding:0em; font-size:12px;" />
                </apex:pageBlockSectionItem>

                 <!--***PAYMENT METHOD***-->
                 <apex:pageBlockSectionItem html-type="myoutercontainer">
                    <apex:outputLabel for="paymentMethod" value="Payment Method"></apex:outputLabel>
                    <apex:outputPanel layout="block">
                        <apex:outputPanel layout="block"></apex:outputPanel>
                        <apex:inputText id="paymentMethod" value="{!defaultPaymentMethodNm}"  style="width:150px; margin-left:0px; padding:0em; font-size:12px;" /> &nbsp;
                        <apex:commandButton value="+" style="padding: 0.5em 1em;" styleClass="show-payment-section-button"></apex:commandButton>
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
              </apex:pageBlockSection>

              <!-- Payment page block section -->
              <apex:pageBlockSection html-type="payment-section" columns="1" collapsible="false" >
                  <apex:pageBlockSectionItem html-type="payment-section-header">
                      <apex:outputLabel value="Payment Method Details :"></apex:outputLabel>
                  </apex:pageBlockSectionItem>

                  <!--***Payment type***-->
                  <apex:pageBlockSectionItem html-type="myoutercontainer">
                    <apex:outputLabel for="paymentType" value="{!$ObjectType.Opportunity.Fields.rC_Giving__Payment_Method__c.Label}"></apex:outputLabel>
                    <apex:selectList id="paymentType" value="{!Opportunity.rC_Giving__Payment_Method__c}" size="1" styleClass="auto_width Payment_Method__c payment-type-select" >
                        <apex:selectOptions Value="{!paymentTypeItems}"></apex:selectOptions>
                    </apex:selectList>
                  </apex:pageBlockSectionItem>

                  <apex:pageBlockSectionItem html-type="payment-section" html-show-on="Cash/Check" >
                    <apex:outputLabel for="firstTransactionComplete" value="First Transaction Complete?"></apex:outputLabel>
                    <apex:inputCheckbox value="{!isFirstTransactionComplete}" style="margin-left:0px; padding:0em; font-size:12px;" />
                </apex:pageBlockSectionItem>

                  <!--***Card Number***-->
                  <apex:pageBlockSectionItem html-type="myoutercontainer" html-show-on="Charge Card">
                    <apex:outputLabel for="cardNumber" value="Card Number"></apex:outputLabel>
                        <apex:inputText id="cardNumber" value="{!cardNumber}" style="margin-left:0px; padding:0em; font-size:12px;"></apex:inputText>
                  </apex:pageBlockSectionItem>

                  <!--***Expiration***-->
                  <apex:pageBlockSectionItem html-type="myoutercontainer" html-show-on="Charge Card">
                    <apex:outputLabel for="month" value="Card Expiration Month"></apex:outputLabel>
                    <apex:outputPanel layout="block">
                        <apex:outputPanel layout="block" />
                        <apex:inputText id="month" value="{!month}" style="width:75px; margin-left:0px; padding:0em; font-size:12px;" />&nbsp;
                    </apex:outputPanel>
                  </apex:pageBlockSectionItem>
                  
                  <apex:pageBlockSectionItem html-type="myoutercontainer" html-show-on="Charge Card">
                    <apex:outputLabel for="year" value="Card Expiration Year"></apex:outputLabel>
                    <apex:outputPanel layout="block">
                        <apex:outputPanel layout="block" />
                        <apex:inputText id="year" value="{!year}" onkeypress="return isNumberKeyGivingAmount(event, id)" style="width:50px; margin-left:0px; padding:0em; font-size:12px;" />
                    </apex:outputPanel>
                  </apex:pageBlockSectionItem>

                  <!--***CVV***-->
                  <apex:pageBlockSectionItem html-type="myoutercontainer" html-show-on="Charge Card">
                    <apex:outputLabel for="cvv" value="CVV"></apex:outputLabel>
                    <apex:inputText id="cvv" value="{!cvv}" style="width:75px; margin-left:0px; padding:0em; font-size:12px;" />
                  </apex:pageBlockSectionItem>

               <!--***Name***-->
                <apex:pageBlockSectionItem html-type="myoutercontainer" html-show-on="Charge Card">
                    <apex:outputLabel for="cardHolderName" value="Card Holder Name"></apex:outputLabel>
                    <apex:outputPanel layout="block" >
                        <apex:outputPanel layout="block" />
                        <apex:inputText id="cardHolderName" value="{!cardHolderName}" style="width:150px; margin-left:0px; padding:0em; font-size:12px;"/> &nbsp;
<!--                         <apex:inputText id="lastName" value="{!lastName}" style="width:150px; margin-left:0px; padding:0em; font-size:12px;"/> -->
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>

                <!--***Billing Address Street***-->
                  <apex:pageBlockSectionItem html-type="myoutercontainer" html-show-on="Charge Card">
                    <apex:outputLabel for="street" value="Billing Street"></apex:outputLabel>
                    <apex:outputPanel layout="block">
                        <apex:outputPanel layout="block" />
                        <apex:inputText id="street" value="{!street}" style="width:125px; margin-left:0px; padding:0em; font-size:12px;" />
                         &nbsp;
                        <apex:commandButton value="Copy Billing Address" 
                                                action="{!updatePaymentMethodBillingAddressFromAccount}" 
                                                status="addressStatus"
                                                reRender="payment-section, giving_wizard" />
                         <apex:actionStatus id="addressStatus">
                         <apex:facet name="start">
                             <apex:image value="/img/loading.gif" style="vertical-align: middle;" />
                         </apex:facet>
                     </apex:actionStatus>
                    </apex:outputPanel>
                   
                </apex:pageBlockSectionItem>

                 <!--  City -->
                 <apex:pageBlockSectionItem html-type="myoutercontainer" html-show-on="Charge Card">
                    <apex:outputLabel for="city" value="Billing City" />
                    <apex:inputText id="city" value="{!city}" style="width:100px; margin-left:0px; padding:0em; font-size:12px;" />
                </apex:pageBlockSectionItem>

                <!--***State PostCode***-->
                <apex:pageBlockSectionItem html-type="myoutercontainer" html-show-on="Charge Card">
                    <apex:outputLabel for="state" value="Billing State" />
                    <apex:outputPanel layout="block">
                        <apex:outputPanel layout="block" />
                        <apex:inputText id="state" value="{!state}" style="width:100px; margin-left:0px; padding:0em; font-size:12px;" /> &nbsp;
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem html-type="myoutercontainer" html-show-on="Charge Card">
                    <apex:outputLabel for="postCode" value="Billing Postal Code" />
                    <apex:outputPanel layout="block">
                        <apex:outputPanel layout="block" />
                        <apex:inputText id="postCode" value="{!postCode}" style="width:100px; margin-left:0px; padding:0em; font-size:12px;" />
                    </apex:outputPanel>
                </apex:pageBlockSectionItem>

                <!--  Country -->
                 <apex:pageBlockSectionItem html-type="myoutercontainer" html-show-on="Charge Card">
                    <apex:outputLabel for="country" value="Billing Country" />
                    <apex:inputText id="country" value="{!country}" style="width:90px; margin-left:0px; padding:0em; font-size:12px;" />
                </apex:pageBlockSectionItem>
              </apex:pageBlockSection>
         </apex:pageBlock>
         <!-- Page To be displayed on + click //rendered="{!showBoolBlk}"
               <apex:pageBlock html-class="payment-entry-panel">
          </apex:pageBlock> -->
       </apex:form>

       <script>
          var jQ = jQuery.noConflict();

          jQ(document).ready(function(){
              //hide the panel by default
                jQ("[type='payment-section']").hide();

                //hide all attributes with show-on attribute on it
                jQ("[show-on]").hide();

                jQ(".show-payment-section-button").click(function(event){
                    jQ("[type='payment-section']").toggle();

                    if(  jQ("[type='payment-section']").is(":visible") ){

                        //Clear default payment value if any, take backup though
                        var paymentValue = jQ('[id$="paymentMethod"]').val();

                        if(paymentValue){
                            jQ('[id$="paymentMethod"]').attr('default-payment',paymentValue);
                            jQ('[id$="paymentMethod"]').val("");
                        }

                        jQ("body")[0].scrollTop = jQ(this).offset().top;

                    }
                    else{

                       var paymentValue = jQ('[id$="paymentMethod"]').attr('default-payment');

                        if(paymentValue){
                            jQ('[id$="paymentMethod"]').val(paymentValue);
                        }
                    }

                  return false;
              });

              jQ(".payment-type-select").click(function(event){
                  var selectedValue = jQ(this).val();
                  jQ("[show-on]").hide();
                  jQ("[show-on='"+selectedValue+"']").show();
                  return false;
              });

              //code for switch btn
              jQ('.switch-to-adv-btn').click(function(event) {
                  if(jQ("[type='payment-section']").is(":visible")) { //if payment method section is visible
                      var paymentMethodValue = jQ('[id$="paymentType"]').val();
 
                      if((paymentMethodValue == 'Charge Card' && jQ('[id$="cardNumber"]').val() != '') || paymentMethodValue == 'Cash/Check') { //if payment method contain some value
                         if (confirm('Due to security reasons, new payment data cannot be saved when switching to advanced edit mode. You will lose the data.') ) {
                            return true;
                         }
                         event.preventDefault();
                     }
                  }
              });
          });

          //code to validate number
          function isNumberKeyGivingAmount(evt, id) {
               var charCode = (evt.which) ? evt.which : evt.keyCode
               var value = document.getElementById(id).value;
               //alert(value);
               if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                   if(charCode == 46 && value.indexOf('.') < 0)
                       return true;
                   else
                       return false;
               }
               return true;
          }

          //Code for required fields
          function isNotEmpty() {
              var accountVal = document.getElementById("accountName").value;
              var dateVal = document.getElementById("date").value;
              var amtVal = document.getElementById("amount").value;
              var campVal = document.getElementById("campaign").value;

              if(accountVal == '' || dateVal == '' || amtVal == '' || campVal == '')
                  return false;
              if(accountVal == null || dateVal == null || amtVal == null || campVal == null)
                 return false;

              return true;
          }

          function notEmpty() {
               alert(document.getElementById('{!$Component.pgVolReg:document:paymentMethod}').value);
              //  var myTextField = document.getElementById('{!$Component.pgVolReg:document:paymentMethod}').value;
               // if(myTextField != "")
              //      alert("You entered: " + myTextField.value)
              //  else
              //      alert("Would you please enter some text?")
            }

    </script>
</apex:page>