<apex:page standardController="Campaign" extensions="rC_Giving.Campaign_EmbedKPI" action="{!initialize}" sidebar="false" showHeader="false">
    <style type="text/css">
body {
    vertical-align: top;
}

.mainTable {
    vertical-align: top;
    display: inline-block;
    width: 45%;
}

.mainTable.column-1 {
    margin-right: 2.5%;
}

.mainTable.column-2 {
    margin-left: 2.5%;
}

.mainTable td.labels {
    text-align: right;
    white-space: nowrap;
    width: 15%;
}

.mainTable td.labels .upperLabel {
    font-size: 100%;
    font-weight: bold;
    display: block;
}

.mainTable td.labels .lowerLabel {
    font-size: 80%;
    color: #bbb;
    display: block;
}

.mainTable td.lights {
    text-align: right;
    white-space: nowrap;
    width: 10%;
    padding: 0.5em;
    vertical-align: middle !important;
    font-size: 90%;
    font-weight: bold;
}

.mainTable td.lights img {
    vertical-align: middle;
}

.mainTable td.center {
    padding: 0.5em;
}

.mainTable td.center .container {
    position: relative;
    height: 2em;
    vertical-align: middle;
}

.mainTable td.center .container .q1 {
    position: absolute;
    z-index: -1;
    background-color: #865F11;
    left: 0;
    top: 0%;
    bottom: 0%;
    right: 80%;
    opacity: 0.35;
}

.mainTable td.center .container .q2 {
    position: absolute;
    z-index: -2;
    background-color: #CC9933;
    left: 20%;
    top: 0%;
    bottom: 0%;
    right: 60%;
    opacity: 0.35;
}

.mainTable td.center .container .q3 {
    position: absolute;
    z-index: -3;
    background-color: #E6C788;
    left: 40%;
    top: 0;
    bottom: 0;
    right: 40%;
    opacity: 0.35;
}

.mainTable td.center .container .q4 {
    position: absolute;
    z-index: 3;
    left: 60%;
    top: 10%;
    bottom: 10%;
    right: 20%;
    border-right: 1px solid black;
    text-align: right;
    font-size: 80%;
    font-color: #ccc;
    vertical-align: middle;
    padding-right: 1em;
    padding-top: 1.5em;
}

.mainTable td.center .container .actual {
    position: absolute;
    z-index: 1;
    left: 0;
    top: 40%;
    bottom: 40%;
    background-color: #9A7D43;
}

.mainTable td.center .container .actual.overmax {
    background-color: #618A3C;
}

.mainTable td.center .container .actual .exceeds-text {
    display: block;
    padding-top: 0.5em;
    font-size: 80%;
    text-align: right;
    margin-right: 12%;
}

.legend {
    padding: 1em 0em;
    padding-left: 7.5%;
    text-align: left;
}

.legend .group {
    display: inline-block;
}

.legend .group .label {
    padding: 0.5em;
    font-size: 80%;
    color: #555;
}

.legend .group .box {
    display: inline-block;
    height: 15px;
    width: 15px;
    opacity: 0.35;
    border: 1px solid black;
}

.legend .group.q1 .box {
    background-color: #865F11;
}

.legend .group.q2 .box {
    background-color: #CC9933;
}

.legend .group.q3 .box {
    background-color: #E6C788;
}

.legend .group.q4 .box {
    background-color: white;
}
</style>

    <apex:dataTable styleClass="mainTable column-1" var="bulletChartData" value="{!bulletChartDataList}" width="100%">
        <apex:column styleClass="labels" width="15%">
            <apex:outputText styleClass="upperLabel" value="{0}">
                <apex:param value="{!bulletChartData.upperLabel}" />
            </apex:outputText>

            <apex:outputText styleClass="lowerLabel" value="{0}">
                <apex:param value="{!bulletChartData.lowerLabel}" />
            </apex:outputText>
        </apex:column>

        <apex:column styleClass="center">
            <apex:outputPanel layout="block" styleClass="container" style="position: relative;" rendered="{!NOT(BLANKVALUE(bulletChartData.target, 0) == 0)}">
                <apex:outputPanel layout="block" styleClass="actual {!IF(bulletChartData.ratio> 1.10, 'overmax', '')}" style="right: {!MAX(10, 100 - ROUND(100 * 0.8 * bulletChartData.ratio, 2))}%;">
                    <apex:outputText styleClass="exceeds-text" rendered="{!bulletChartData.ratio > 1.10}" value="Exceeds 110%" />
                </apex:outputPanel>

                <div class="q1" title="0-25% of target">&nbsp;</div>
                <div class="q2" title="25-50% of target">&nbsp;</div>
                <div class="q3" title="50-75% of target">&nbsp;</div>
                <div class="q4" title="75%+ of target">&nbsp;</div>
                <div class=""><span></span></div>
            </apex:outputPanel>

            <apex:outputPanel layout="block" styleClass="container" style="position: relative;" rendered="{!BLANKVALUE(bulletChartData.target, 0) == 0}">
                <div class="q1" title="0-25% of target">&nbsp;</div>
                <div class="q2" title="25-50% of target">&nbsp;</div>
                <div class="q3" title="50-75% of target">&nbsp;</div>
                <div class="q4" title="75%+ of target">&nbsp;</div>
                <div class=""><span></span></div>
            </apex:outputPanel>
        </apex:column>

        <apex:column styleClass="lights">
            <apex:outputPanel style="padding-right: 0.5em;">{!ROUND(100 * bulletChartData.ratio, 2)}%</apex:outputPanel>
            <apex:image rendered="{!NOT(ISBLANK(bulletChartData.trafficUrl))}" value="{!bulletChartData.trafficUrl}" />
        </apex:column>
    </apex:dataTable>

    <apex:dataTable styleClass="mainTable column-2" var="bulletChartData" value="{!bulletChartDataHierarchyList}" width="100%">
        <apex:column styleClass="labels" width="15%">
            <apex:outputText styleClass="upperLabel" value="{0}">
                <apex:param value="{!bulletChartData.upperLabel}" />
            </apex:outputText>

            <apex:outputText styleClass="lowerLabel" value="{0}">
                <apex:param value="{!bulletChartData.lowerLabel}" />
            </apex:outputText>
        </apex:column>

        <apex:column styleClass="center">
            <apex:outputPanel layout="block" styleClass="container" style="position: relative;" rendered="{!NOT(BLANKVALUE(bulletChartData.target, 0) == 0)}">
                <apex:outputPanel layout="block" styleClass="actual {!IF(bulletChartData.ratio> 1.10, 'overmax', '')}" style="right: {!MAX(10, 100 - ROUND(100 * 0.8 * bulletChartData.ratio, 2))}%;">
                    <apex:outputText styleClass="exceeds-text" rendered="{!bulletChartData.ratio > 1.10}" value="Exceeds 110%" />
                </apex:outputPanel>

                <div class="q1" title="0-25% of target">&nbsp;</div>
                <div class="q2" title="25-50% of target">&nbsp;</div>
                <div class="q3" title="50-75% of target">&nbsp;</div>
                <div class="q4" title="75%+ of target">&nbsp;</div>
                <div class=""><span></span></div>
            </apex:outputPanel>

            <apex:outputPanel layout="block" styleClass="container" style="position: relative;" rendered="{!BLANKVALUE(bulletChartData.target, 0) == 0}">
                <div class="q1" title="0-25% of target">&nbsp;</div>
                <div class="q2" title="25-50% of target">&nbsp;</div>
                <div class="q3" title="50-75% of target">&nbsp;</div>
                <div class="q4" title="75%+ of target">&nbsp;</div>
                <div class=""><span></span></div>
            </apex:outputPanel>
        </apex:column>

        <apex:column styleClass="lights">
            <apex:outputPanel style="padding-right: 0.5em;">{!ROUND(100 * bulletChartData.ratio, 2)}%</apex:outputPanel>
            <apex:image rendered="{!NOT(ISBLANK(bulletChartData.trafficUrl))}" value="{!bulletChartData.trafficUrl}" />
        </apex:column>
    </apex:dataTable>

    <apex:outputPanel layout="block" styleClass="legend">
        <apex:outputPanel styleClass="group q1">
            <apex:outputPanel styleClass="box">&nbsp;</apex:outputPanel>
            <apex:outputPanel styleClass="label">0 - 25%</apex:outputPanel>
        </apex:outputPanel>

        <apex:outputPanel styleClass="group q2">
            <apex:outputPanel styleClass="box">&nbsp;</apex:outputPanel>
            <apex:outputPanel styleClass="label">25 - 50%</apex:outputPanel>
        </apex:outputPanel>

        <apex:outputPanel styleClass="group q3">
            <apex:outputPanel styleClass="box">&nbsp;</apex:outputPanel>
            <apex:outputPanel styleClass="label">50 - 75%</apex:outputPanel>
        </apex:outputPanel>

        <apex:outputPanel styleClass="group q4">
            <apex:outputPanel styleClass="box">&nbsp;</apex:outputPanel>
            <apex:outputPanel styleClass="label">75 - 100%</apex:outputPanel>
        </apex:outputPanel>
    </apex:outputPanel>
</apex:page>