/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Campaign_RollupGivingBatchable implements Database.Batchable<SObject>, Database.Stateful {
    global Campaign_RollupGivingBatchable() {

    }
    global void execute(Database.BatchableContext batchableContext, List<Campaign> campaignList) {

    }
    global void finish(Database.BatchableContext batchableContext) {

    }
    global static void schedule() {

    }
    global Database.QueryLocator start(Database.BatchableContext batchableContext) {
        return null;
    }
}
