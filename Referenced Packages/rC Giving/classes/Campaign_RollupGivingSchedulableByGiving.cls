/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Campaign_RollupGivingSchedulableByGiving implements System.Schedulable {
    global Campaign_RollupGivingSchedulableByGiving() {

    }
    global void execute(System.SchedulableContext schedulableContext) {

    }
}
