/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Opportunity_ProcessOverdueSchedulable implements System.Schedulable {
    global Opportunity_ProcessOverdueSchedulable() {

    }
    global void Schedule() {

    }
    global void execute(System.SchedulableContext schedulableContext) {

    }
}
