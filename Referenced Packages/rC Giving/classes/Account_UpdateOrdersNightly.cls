/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Account_UpdateOrdersNightly implements System.Schedulable {
    global Account_UpdateOrdersNightly() {

    }
    global Account_UpdateOrdersNightly(String priceBookEntryId, String groupingType) {

    }
    global void execute(System.SchedulableContext schedulableContext) {

    }
}
