/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class OpportunityRecordTypes {
    global static String RT_CORPORATE;
    global static Id RT_CORPORATE_ID;
    global static String RT_DEVELOPER_NAME_CORPORATE;
    global static String RT_DEVELOPER_NAME_DONATION;
    global static String RT_DEVELOPER_NAME_GRANT;
    global static String RT_DEVELOPER_NAME_INKIND;
    global static String RT_DEVELOPER_NAME_ITEM_PRODUCT;
    global static String RT_DEVELOPER_NAME_MEMBERSHIP;
    global static String RT_DEVELOPER_NAME_OUTRIGHT_GIFT;
    global static String RT_DEVELOPER_NAME_PACKAGE_PRODUCT;
    global static String RT_DEVELOPER_NAME_PLEDGE;
    global static String RT_DEVELOPER_NAME_PLEDGE_PAYMENT;
    global static String RT_DEVELOPER_NAME_PROPOSAL;
    global static String RT_DEVELOPER_NAME_PURCHASE;
    global static String RT_DEVELOPER_NAME_TRANSACTION;
    global static String RT_DONATION;
    global static Id RT_DONATION_ID;
    global static String RT_GRANT;
    global static Id RT_GRANT_ID;
    global static String RT_INKIND;
    global static Id RT_INKIND_ID;
    global static Id RT_ITEM_ID_PRODUCT;
    global static String RT_ITEM_PRODUCT;
    global static String RT_MEMBERSHIP;
    global static Id RT_MEMBERSHIP_ID;
    global static String RT_OUTRIGHT_GIFT;
    global static Id RT_OUTRIGHT_GIFT_ID;
    global static Id RT_PACKAGE_ID_PRODUCT;
    global static String RT_PACKAGE_PRODUCT;
    global static String RT_PLEDGE;
    global static Id RT_PLEDGE_ID;
    global static String RT_PLEDGE_PAYMENT;
    global static Id RT_PLEDGE_PAYMENT_ID;
    global static String RT_PROPOSAL;
    global static Id RT_PROPOSAL_ID;
    global static String RT_PURCHASE;
    global static Id RT_PURCHASE_ID;
    global static String RT_TRANSACTION;
    global static Id RT_TRANSACTION_ID;
    global OpportunityRecordTypes() {

    }
}
