/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Account_RollupAffiliationsSchedulable implements System.Schedulable {
    global Account_RollupAffiliationsSchedulable() {

    }
    global void execute(System.SchedulableContext schedulableContext) {

    }
    global void schedule() {

    }
}
