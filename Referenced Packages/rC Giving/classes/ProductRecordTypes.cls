/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ProductRecordTypes {
    global static String PR_ITEM;
    global static Id PR_ITEM_ID;
    global static String PR_PACKAGE;
    global static Id PR_PACKAGE_ID;
    global ProductRecordTypes() {

    }
}
