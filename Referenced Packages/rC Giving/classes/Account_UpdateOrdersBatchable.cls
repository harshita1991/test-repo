/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Account_UpdateOrdersBatchable implements Database.Batchable<SObject>, Database.Stateful {
    global Account_UpdateOrdersBatchable(String pricebookEntryId, String groupingType) {

    }
    global Account_UpdateOrdersBatchable(String pricebookEntryId, String groupingType, Boolean contractForAll) {

    }
    global void execute(Database.BatchableContext batchableContext, List<SObject> scope) {

    }
    global void finish(Database.BatchableContext batchableContext) {

    }
    global Database.QueryLocator start(Database.BatchableContext batchableContext) {
        return null;
    }
}
