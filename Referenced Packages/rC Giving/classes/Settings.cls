/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Settings {
    global static rC_Giving__Campaign_Setting__c CAMPAIGN_SETTING {
        get;
    }
    global static rC_Giving__Opportunity_Setting__c OPPORTUNITY_SETTING {
        get;
    }
    global static rC_Giving__Payment_Method_Setting__c PAYMENT_METHOD_SETTING {
        get;
    }
    global Settings() {

    }
    global static void initializeCustomSetting(String customSetting, String customSettingField, String value, String securityToken) {

    }
}
