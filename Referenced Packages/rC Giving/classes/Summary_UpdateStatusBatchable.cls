/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Summary_UpdateStatusBatchable implements Database.Batchable<SObject>, Database.Stateful {
    global Summary_UpdateStatusBatchable() {

    }
    global void execute(Database.BatchableContext batchableContext, List<rC_Giving__Summary__c> summaryList) {

    }
    global void finish(Database.BatchableContext batchableContext) {

    }
    global static void schedule() {

    }
    global Database.QueryLocator start(Database.BatchableContext batchableContext) {
        return null;
    }
}
