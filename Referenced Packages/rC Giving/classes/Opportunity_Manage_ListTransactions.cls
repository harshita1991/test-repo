/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Opportunity_Manage_ListTransactions extends rC_Giving.SobjectExtension {
    global String selectedStageName {
        get;
        set;
    }
    global Opportunity_Manage_ListTransactions() {

    }
    global List<System.SelectOption> getStageNameValues() {
        return null;
    }
}
