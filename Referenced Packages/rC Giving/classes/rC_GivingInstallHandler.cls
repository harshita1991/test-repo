/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class rC_GivingInstallHandler implements System.InstallHandler {
    global rC_GivingInstallHandler() {

    }
    global void onInstall(System.InstallContext context) {

    }
    global void runNewOrgScript(System.InstallContext context) {

    }
}
