/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Contact_RollupHardCreditsBatchable implements Database.Batchable<SObject>, Database.Stateful {
    global Contact_RollupHardCreditsBatchable() {

    }
    global void execute(Database.BatchableContext batchableContext, List<Contact> contactList) {

    }
    global void finish(Database.BatchableContext batchableContext) {

    }
    global static void schedule() {

    }
    global Database.QueryLocator start(Database.BatchableContext batchableContext) {
        return null;
    }
}
