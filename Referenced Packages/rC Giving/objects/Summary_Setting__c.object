<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>Hierarchy</customSettingsType>
    <description>Stores custom settings related to the Summary Object.</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>Disable_All__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, all Summary triggers are disabled.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, all Summary triggers are disabled.</inlineHelpText>
        <label>Disable All Triggers?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Disable_CascadeAccounts__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, disables Summary_CascadeAccounts2, which performs cascade of &quot;Custom Expiration Date&quot; field value from Summary to &quot;Primary Membership Expiration Date&quot; field on it&apos;s Accounts. If null is specified ,then system rolls-up &quot;Giving Level Expiration Date&quot; value from Summary</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, &quot;Custom Expiration Date&quot; field updates on Summary will not roll-up to &quot;Primary Membership Expiration Date&quot; field on it&apos;s Account.</inlineHelpText>
        <label>Disable Cascade Accounts?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Disable_RollupGiving__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Deprecated: If checked, disables RollupGiving, which performs rollup to forecast category fields on the parent giving. RollupGiving resets the amount on related fields of Giving to zero, sets the Rollup Giving time to current date/time, finds all transactions belonging to a giving, and loops over transactions and applies amounts by forecast category to parent giving.</description>
        <externalId>false</externalId>
        <inlineHelpText>@Deprecated: Trigger/Class was deleted as of Giving 1.7</inlineHelpText>
        <label>Disable Rollup Giving?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Disable_Summary_Recal_On_Expiry_Update__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, disables the trigger which recalculates the summary when &quot;Custom Expiration Date&quot; is updated.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, disables the trigger which recalculates the summary when &quot;Custom Expiration Date&quot; is updated.</inlineHelpText>
        <label>Disable Summary Recal On Expiry Update</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Ignore_Multiple_Currencies_Error__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Normally, only one currency type is allowed to be aggregated per summary. This flag (if set to true) will skip the multiple-currency error message.</description>
        <externalId>false</externalId>
        <inlineHelpText>Normally, only one currency type is allowed to be aggregated per summary. This flag (if set to true) will skip the multiple-currency error message.</inlineHelpText>
        <label>Ignore Multiple Currencies Error?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Summary_Expiration_Date__c</fullName>
        <defaultValue>&quot;Pledge/Payment Date&quot;</defaultValue>
        <deprecated>false</deprecated>
        <description>Allows administrator to choose the date on which the &quot;Expiration Date&quot; will fall.</description>
        <externalId>false</externalId>
        <inlineHelpText>Valid Values:
Pledge/Payment Date
First of the Month
End of the Month</inlineHelpText>
        <label>Summary Expiration Date</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Track_Value_1__c</fullName>
        <deprecated>false</deprecated>
        <description>It defaults to Opportunity.Giving_Type__c and then concats with &quot;Track Value 3&quot; + &quot;Track Value 2&quot; fields</description>
        <externalId>false</externalId>
        <inlineHelpText>Specify the field on Opportunity to be copied over on Giving Channel field in Summary. Default value is  Opportunity.rc_Giving__Giving_Type__c</inlineHelpText>
        <label>Track Value 1</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Track_Value_2__c</fullName>
        <deprecated>false</deprecated>
        <description>It defaults to Opportunity.Campaign_Channel__c and then concats with &quot;Track Value 1&quot; + &quot;Track Value 3&quot; field values.</description>
        <externalId>false</externalId>
        <inlineHelpText>Specify the field on Opportunity to be copied over on Giving Channel field in Summary. It defaults to pportunity.rc_Giving__Campaign_Channel__c</inlineHelpText>
        <label>Track Value 2</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Track_Value_3__c</fullName>
        <deprecated>false</deprecated>
        <description>It defaults to &quot;by&quot; then concats as &quot;Track Value 1&quot; + &quot;Track Value 3&quot; + &quot;Track Value 2&quot; values.</description>
        <externalId>false</externalId>
        <inlineHelpText>Specify a word that concatenates &quot;Track Value 1&quot; and &quot;Track Value 2&quot;.</inlineHelpText>
        <label>Track Value 3</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Update_Status_Batch_Delay__c</fullName>
        <defaultValue>5</defaultValue>
        <deprecated>false</deprecated>
        <description>Specifies the number of minutes to wait before running the batch for updating giving level status on Summary. Number can be specified as values which will be calculated as minutes, ex: A value of  5 means job will wait 5 minutes before rescheduling itself. If null is specified the job will fail with an error.</description>
        <externalId>false</externalId>
        <inlineHelpText>Specifies the number of minutes to wait before running the batch for updating giving level status on Summary.</inlineHelpText>
        <label>Update Status Batch Delay</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Update_Status_Batch_Size__c</fullName>
        <defaultValue>200</defaultValue>
        <deprecated>false</deprecated>
        <description>Specify batch size between 1 to 200.If the batch size is specified as more than 200 or less than 1 then batch job will be executed by considering it as 200.</description>
        <externalId>false</externalId>
        <inlineHelpText>Specifies the batch size(1-200) for Summary_UpdateGivingLevelStatusBatchable batch.</inlineHelpText>
        <label>Update Status Batch Size</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Update_Status_Batch__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, giving level status on Summary will be inserted/updated by a batch job.Make sure &apos;Update Status Batch Delay&apos; and &apos;Update Status Batch Size&apos; contains valid data.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, giving level status on Summary will be inserted/updated by a batch job.</inlineHelpText>
        <label>Update Status Batch?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Upgrade_Reset__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>True: membership summary dates will be updated when a donor moves up giving level(s) 
False: membership summary dates will not be updated when a donor moves up giving level(s)</description>
        <externalId>false</externalId>
        <inlineHelpText>Check if you want membership summary dates to be updated when a donor moves up giving level(s)</inlineHelpText>
        <label>Upgrade Reset?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Summary Setting</label>
    <visibility>Public</visibility>
</CustomObject>
