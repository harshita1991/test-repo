<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Items are products the organization sells or gives away. Supports a parent child relationship to track Items that come in variations such as size or color.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Active__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Manually populated field indicating is this Item active?</description>
        <externalId>false</externalId>
        <inlineHelpText>Is this Item active?</inlineHelpText>
        <label>Active?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Archive_Flag__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Identifies a record to be archived in roundData and removed from Salesforce. It will reside in roundData for storage and query runs.</description>
        <externalId>false</externalId>
        <inlineHelpText>Identifies a record to be archived in roundData and removed from Salesforce. It will reside in roundData for storage and query runs.</inlineHelpText>
        <label>Archive?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Delivery_Method__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated field indicating how this item will be delivered.</description>
        <externalId>false</externalId>
        <inlineHelpText>Indicates how this item will be delivered.</inlineHelpText>
        <label>Delivery Method</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Email</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Billing Address</fullName>
                    <default>true</default>
                </value>
                <value>
                    <fullName>Shipping Address</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Mailing Address</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>Other Address</fullName>
                    <default>false</default>
                </value>
                <value>
                    <fullName>In Person</fullName>
                    <default>false</default>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Giving_Level__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated field indicating the giving level at which this item is available.</description>
        <externalId>false</externalId>
        <inlineHelpText>The giving level at which this item is available.</inlineHelpText>
        <label>Giving Level</label>
        <referenceTo>Giving_Level__c</referenceTo>
        <relationshipLabel>Giving Level Items</relationshipLabel>
        <relationshipName>Items</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>On_Add__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Manually populated field indicating this item will be sent when the giving type of additional is selected on a related gift.</description>
        <externalId>false</externalId>
        <inlineHelpText>Item will be sent when the giving type of additional is selected on a related gift.</inlineHelpText>
        <label>On Add?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>On_New__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Manually populated field indicating this item will be sent when a new membership is selected.</description>
        <externalId>false</externalId>
        <inlineHelpText>Item will be sent when a new membership is selected.</inlineHelpText>
        <label>On New?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>On_Pending__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Manually populated field indicating this item will be sent when a gift status of pending is indicated.</description>
        <externalId>false</externalId>
        <inlineHelpText>Item will be sent when when a gift status of pending is indicated.</inlineHelpText>
        <label>On Pending?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>On_Rejoin__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Manually populated field indicating this item will be sent when a donor rejoins your organization.</description>
        <externalId>false</externalId>
        <inlineHelpText>Item will be sent when a donor rejoins your organization.</inlineHelpText>
        <label>On Rejoin?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>On_Renew__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Manually populated field indicating this item will be sent when a membership is renewed.</description>
        <externalId>false</externalId>
        <inlineHelpText>Item will be sent when a membership is renewed.</inlineHelpText>
        <label>On Renew?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>On_Upgrade__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Manually populated field indicating this item will be sent when a donor upgrades their membership level.</description>
        <externalId>false</externalId>
        <inlineHelpText>Item will be sent when a donor upgrades their membership level.</inlineHelpText>
        <label>On Upgrade?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Product_Code__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated field indicating the Product Code this item is assocuated with.</description>
        <externalId>false</externalId>
        <formula>Product__r.ProductCode</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The Product Code this item is assocuated with.</inlineHelpText>
        <label>Product Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product_Description__c</fullName>
        <deprecated>false</deprecated>
        <description>Manually populated field indicating the Product Description  for this item.</description>
        <externalId>false</externalId>
        <formula>Product__r.Description</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <inlineHelpText>The Product Description  for this item.</inlineHelpText>
        <label>Product Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <description>Manually populated field indicating the Product this item is assocuated with.</description>
        <externalId>false</externalId>
        <inlineHelpText>The Product this item is assocuated with.</inlineHelpText>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Giving Level Items</relationshipLabel>
        <relationshipName>Giving_Levels</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Sustainer__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Manually populated field indicating when this is set to true, it applies to New , Renew and Rejoin Sustainer Gifts.</description>
        <externalId>false</externalId>
        <inlineHelpText>When this is set to true, it applies to New , Renew and Rejoin Sustainer Gifts.</inlineHelpText>
        <label>Sustainer?</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Giving Level Item</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Giving_Level__c</columns>
        <columns>Product_Code__c</columns>
        <columns>On_Add__c</columns>
        <columns>On_New__c</columns>
        <columns>On_Pending__c</columns>
        <columns>On_Rejoin__c</columns>
        <columns>On_Renew__c</columns>
        <columns>On_Upgrade__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <displayFormat>{00000000}</displayFormat>
        <label>Reference #</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Giving Level Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
