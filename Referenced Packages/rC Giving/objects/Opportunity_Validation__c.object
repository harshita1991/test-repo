<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <customSettingsType>Hierarchy</customSettingsType>
    <description>Stores custom settings related to the Opportunity Validation</description>
    <enableFeeds>false</enableFeeds>
    <fields>
        <fullName>Account_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>When unchcked, turns off validation rule Account Required, which states that all Opportunities must have an Account before Save.</description>
        <externalId>false</externalId>
        <inlineHelpText>When unchecked, Opportunities will not require an Account.</inlineHelpText>
        <label>Account Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Activity_Type_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Activity Type&quot; can only be populated on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Activity Type&quot; can be populated on records irrespective of &quot;Is Giving&quot;.</inlineHelpText>
        <label>Activity Type Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Activity_Type_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Activity Type&quot; is required on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Activity Type&quot; can be left blank when &quot;Is Giving&quot; is checked.</inlineHelpText>
        <label>Activity Type Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Affiliation_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Affiliation&quot; is required. &quot;Affiliation&quot; is populated through the selection of a Campaign which has an &quot;Affiliation&quot; defined.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Affiliation&quot; can be left blank.</inlineHelpText>
        <label>Affiliation Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Amount_Invalid_On_Payment__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Amount&quot; must be zero or positive on a Transaction Payment record.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Amount&quot; can be negative on a Transaction Payment record.</inlineHelpText>
        <label>Amount Invalid On Payment</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Amount_Invalid_On_Refund__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Amount&quot; must be zero or negative on a Transaction Refund record.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Amount&quot; can be anything on a Transaction Refund record.</inlineHelpText>
        <label>Amount Invalid On Refund</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Anniversary_Date_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Anniversary Date&quot; can only be populated on records &quot;Is Giving&quot; or &quot;Is Sustainer&quot; are checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Anniversary Date&quot; can be populated on all records.</inlineHelpText>
        <label>Anniversary Date Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Anniversary_Date_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Anniversary Date&quot; is required on records where &quot;Is Giving&quot; and &quot;Is Sustainer&quot; are checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Anniversary Date&quot; will not be required field when &quot;Is Giving&quot; and &quot;Is Sustainer&quot; are checked.</inlineHelpText>
        <label>Anniversary Date Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Ask_Readiness_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Ask Readiness&quot; must be blank, &apos;Hot&apos;, &apos;Warm&apos;, or &apos;Cold&apos;.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Ask Readiness&quot; can have any value.</inlineHelpText>
        <label>Ask Readiness Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Calculated_Giving_Type_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Calculated Giving Type&quot; must be blank, &apos;New&apos;, &apos;Additional&apos;, &apos;Upgrade&apos;, &apos;Rejoin&apos;, or &apos;Pending&apos;.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Calculated Giving Type&quot; can have any value.</inlineHelpText>
        <label>Calculated Giving Type Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Calculated_Giving_Type_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Calculated Giving Type&quot; can only be populated on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Calculated Giving Type&quot; can be populated on all records.</inlineHelpText>
        <label>Calculated Giving Type Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Campaign_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Not implemented.</description>
        <externalId>false</externalId>
        <inlineHelpText>Not implemented.</inlineHelpText>
        <label>Campaign Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Campaign_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Not implemented.</description>
        <externalId>false</externalId>
        <inlineHelpText>Not implemented.</inlineHelpText>
        <label>Campaign Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Campaign_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Campaign&quot; is required if &quot;Is Giving Transaction&quot; is unchecked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Campaign&quot; will be required if &quot;Is Giving Transaction&quot; is unchecked.</inlineHelpText>
        <label>Campaign Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Close_Date_Prior_To_Parent__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Close Date&quot; of Transaction record cannot be prior to parent Giving record &quot;Close Date&quot;.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Close Date&quot; of Transaction record can be prior to parent Giving record &quot;Close Date&quot;.</inlineHelpText>
        <label>Close Date Prior To Parent</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Current_Giving_Amount_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Current Giving Amount&quot; cannot be blank or less than zero on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Current Giving Amount&quot; can be blank or less than zero on records where &quot;Is Giving&quot; is checked.</inlineHelpText>
        <label>Current Giving Amount Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Giving_Amount_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Giving Amount&quot; can only be populated on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Giving Amount&quot; can only be populated on all records irrespective of &quot;Is Giving&quot; flag.</inlineHelpText>
        <label>Giving Amount Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Giving_Amount_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Giving Amount&quot; cannot be blank on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Giving Amount&quot; can be blank on records where &quot;Is Giving&quot; is checked.</inlineHelpText>
        <label>Giving Amount Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Giving_End_Date_After_Close_Date__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>@Deprecated(Version=2.6)</description>
        <externalId>false</externalId>
        <label>@Deprecated(Version=2.6)</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Giving_End_Date_Required_Blank__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>@Deprecated(Version=2.6)</description>
        <externalId>false</externalId>
        <label>@Deprecated(Version=2.6)</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Giving_Frequency_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Giving Frequency&quot; must be &apos;One Payment&apos;, &apos;Annually&apos;, &apos;Semi-Annual&apos;, &apos;Quarterly&apos;, &apos;Monthly&apos;, &apos;Bi-Monthly&apos;, &apos;Weekly&apos;, &apos;Total&apos; or blank.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Giving Frequency&quot; related check will not be done.</inlineHelpText>
        <label>Giving Frequency Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Giving_Frequency_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Giving Frequency&quot; can only be populated on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Giving Frequency&quot; can be populated on all records irrespective of &quot;Is Giving&quot; flag.</inlineHelpText>
        <label>Giving Frequency Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Giving_Frequency_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Giving Frequency&quot; is required on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Giving Frequency&quot; will not be required field on records where &quot;Is Giving&quot; is checked.</inlineHelpText>
        <label>Giving Frequency Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Giving_Is_Not_Giving_Transaction__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Is Giving&quot; and &quot;Is Giving Transaction&quot; cannot both be checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Is Giving&quot; and &quot;Is Giving Transaction&quot; can both be checked.</inlineHelpText>
        <label>Giving Is Not Giving Transaction</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Giving_Type_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Giving Type&quot; must be &apos;Pending&apos;, &apos;New&apos;, &apos;Additional&apos;, &apos;Upgrade&apos;, &apos;Renewal&apos;, &apos;Rejoin&apos; or blank.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Giving Type&quot; can hold any value.</inlineHelpText>
        <label>Giving Type Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Giving_Type_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Giving Frequency&quot; can only be populated on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Giving Frequency&quot; can be populated on all records irrespective of &quot;Is Giving&quot; flag.</inlineHelpText>
        <label>Giving Type Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Media_1_Amount_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, the corresponding &quot;Media Amount&quot; field must be populated when a &quot;Media Type&quot; field is populated. These fields are used for underwriting Giving records.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, the corresponding &quot;Media Amount&quot; field can be populated or can be left blank irrespective of &quot;Media Type&quot; field.</inlineHelpText>
        <label>Media 1 Amount Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Media_1_Type_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Media Type&quot; must be &apos;Event&apos;, &apos;Print&apos;, &apos;Radio&apos;, &apos;TV&apos;, &apos;Web&apos;, or blank.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Media Type&quot; can have any value.</inlineHelpText>
        <label>Media 1 Type Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Media_2_Amount_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, the corresponding &quot;Media Amount&quot; field must be populated when a &quot;Media Type&quot; field is populated. These fields are used for underwriting Giving records.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, the corresponding &quot;Media Amount&quot; field can be populated or can be left blank irrespective of &quot;Media Type&quot; field.</inlineHelpText>
        <label>Media 2 Amount Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Media_2_Type_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Media Type&quot; must be &apos;Event&apos;, &apos;Print&apos;, &apos;Radio&apos;, &apos;TV&apos;, &apos;Web&apos;, or blank.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Media Type&quot; can hold any value.</inlineHelpText>
        <label>Media 2 Type Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Media_3_Amount_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, the corresponding &quot;Media Amount&quot; field must be populated when a &quot;Media Type&quot; field is populated. These fields are used for underwriting Giving records.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, the corresponding &quot;Media Amount&quot; field can be populated or can be left blank irrespective of &quot;Media Type&quot; field.</inlineHelpText>
        <label>Media 3 Amount Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Media_3_Type_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Media Type&quot; must be &apos;Event&apos;, &apos;Print&apos;, &apos;Radio&apos;, &apos;TV&apos;, &apos;Web&apos;, or blank.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Media Type&quot; can hold any value.</inlineHelpText>
        <label>Media 3 Type Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Media_4_Amount_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, the corresponding &quot;Media Amount&quot; field must be populated when a &quot;Media Type&quot; field is populated. These fields are used for underwriting Giving records.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, the corresponding &quot;Media Amount&quot; field can be populated or can be left blank irrespective of &quot;Media Type&quot; field.</inlineHelpText>
        <label>Media 4 Amount Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Media_4_Type_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Media Type&quot; must be &apos;Event&apos;, &apos;Print&apos;, &apos;Radio&apos;, &apos;TV&apos;, &apos;Web&apos;, or blank.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Media Type&quot; can hold any value.</inlineHelpText>
        <label>Media 4 Type Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Media_5_Amount_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, the corresponding &quot;Media Amount&quot; field must be populated when a &quot;Media Type&quot; field is populated. These fields are used for underwriting Giving records.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, the corresponding &quot;Media Amount&quot; field can be populated or can be left blank irrespective of &quot;Media Type&quot; field.</inlineHelpText>
        <label>Media 5 Amount Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Media_5_Type_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Media Type&quot; must be &apos;Event&apos;, &apos;Print&apos;, &apos;Radio&apos;, &apos;TV&apos;, &apos;Web&apos;, or blank.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Media Type&quot; can hold any value.</inlineHelpText>
        <label>Media 5 Type Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>One_Payments_Cannot_Be_Sustainers__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, records where &quot;Is Giving&quot; and &quot;Is Sustainer&quot; are checked cannot have a &quot;Giving Frequency&quot; of &apos;One Payment.&apos;</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, records where &quot;Is Giving&quot; and &quot;Is Sustainer&quot; are checked can have a &quot;Giving Frequency&quot; of  &apos;One Payment.&apos;</inlineHelpText>
        <label>One Payments Cannot Be Sustainers</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment_Count_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Payment Count&quot; can only be populated on records where &quot;Is Giving&quot; is checked and &quot;Giving Frequency&quot; is &apos;Irregular&apos;.</description>
        <externalId>false</externalId>
        <inlineHelpText>Checks for blank Payment Count field when handling non-Irregular memberships/donations.</inlineHelpText>
        <label>Payment Count Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment_Count_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Payment Count&quot; must be populated when &quot;Giving Frequency&quot; is &quot;Irregular&apos;.</description>
        <externalId>false</externalId>
        <inlineHelpText>Checks for required Payment Count field when handling Irregular memberships/donations.</inlineHelpText>
        <label>Payment Count Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment_Day_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Payment Day&quot; must be &apos;Default&apos;, &apos;1st&apos;, &apos;15th&apos;, &apos;28th&apos;, or blank.</description>
        <externalId>false</externalId>
        <inlineHelpText>If checked, &quot;Payment Day&quot; can hold any value.</inlineHelpText>
        <label>Payment Day Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment_End_Date_After_Close_Date__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Payment End Date&quot; must be before &quot;Close Date&apos; if both fields are populated.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Payment End Date&quot; can be before or after &quot;Close Date&apos;.</inlineHelpText>
        <label>Payment End Date After Close Date</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment_End_Date_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Payment End Date&quot; can only be populated on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Payment End Date&quot; can be populated on all records irrespective of &quot;Is Giving&quot; flag.</inlineHelpText>
        <label>Payment End Date Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment_Frequency_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Payment Frequency&quot; must be &apos;One Payment&apos;, &apos;Annually&apos;, &apos;Semi-Annual&apos;, &apos;Quarterly&apos;, &apos;Monthly&apos;, &apos;Bi-Monthly&apos;, &apos;Weekly&apos;, &apos;Total&apos; or blank.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Payment Frequency&quot; can hold any value.</inlineHelpText>
        <label>Payment Frequency Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment_Frequency_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Payment Frequency&quot; can only be populated on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Payment Frequency&quot; can be populated on all records irrespective of &quot;Is Giving&quot; flag.</inlineHelpText>
        <label>Payment Frequency Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment_Frequency_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Payment Frequency&quot; must be populated on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Payment Frequency&quot; can be left blank on records irrespective of &quot;Is Giving&quot; flag.</inlineHelpText>
        <label>Payment Frequency Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment_Method_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Payment Method&quot; must be &apos;Cash/Check&apos;, &apos;Charge Card&apos;, &apos;EFT&apos;, &apos;Other&apos;, &apos;Paypal&apos;, &apos;Third Party Charge&apos;, &apos;Voucher&apos;, or blank.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Payment Method&quot; can hold any value.</inlineHelpText>
        <label>Payment Method Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment_Method_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Payment Method&quot; can only be populated on records where &quot;Is Giving&quot; is checked and &quot;Transaction Type&quot; is not &apos;Refund&apos;.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Payment Method&quot; can be populated on all records irrespective of &quot;Is Giving&quot; and &quot;Transaction Type&quot;.</inlineHelpText>
        <label>Payment Method Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment_Method_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Payment Method&quot; must be populated when &quot;StageName&quot; is &apos;Completed&apos; or &apos;Pending&apos;.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Payment Method&quot; can be populated or left blank irrespective of &quot;StageName&quot;.</inlineHelpText>
        <label>Payment Method Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment_Refunded_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Is Payment Refunded&quot; can only be checked on records where &quot;Transaction Type&quot; is &apos;Payment&apos;. (A &apos;Refund&apos; cannot be refunded.)</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Is Payment Refunded&quot; can be checked on all records irrespective of &quot;Transaction Type&quot;.</inlineHelpText>
        <label>Payment Refunded Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Refund_Method_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Deprecated.</description>
        <externalId>false</externalId>
        <inlineHelpText>Deprecated.</inlineHelpText>
        <label>Refund Method Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Refund_Method_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>Deprecated.</description>
        <externalId>false</externalId>
        <inlineHelpText>Deprecated.</inlineHelpText>
        <label>Refund Method Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Refund_Reason_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Refund Reason&quot; can only be populated on records where &quot;Transaction Type&quot; is &apos;Refund&apos;.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Refund Reason&quot; can be populated on all records irrespective of &quot;Transaction Type&quot;.</inlineHelpText>
        <label>Refund Reason Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Refundable_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Is Refundable&quot; can only be populated on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Is Refundable&quot; can be populated on all records irrespective of  &quot;Is Giving&quot; flag.</inlineHelpText>
        <label>Refundable Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Selected_Must_Be_False__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Is Selected&quot; must be blank, to support internal system function.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, this will turn of the validation which states that &quot;Is Selected&quot; must be blank, to support internal system function.</inlineHelpText>
        <label>Selected Must Be False</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Suspension_Start_Date_After_End_Date__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Suspended Start Date&quot; must be before &quot;Suspended End Date&quot;.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Suspended Start Date&quot; can be before or after &quot;Suspended End Date&quot;.</inlineHelpText>
        <label>Suspension Start Date After End Date</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Sustainer_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Is Sustainer&quot; can only be checked on records where &quot;Is Giving&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Is Sustainer&quot; can be checked on all records irrespective of &quot;Is Giving&quot; flag.</inlineHelpText>
        <label>Sustainer Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Transaction_Required_Parent_Giving__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Giving Parent&quot; is required on records where &quot;Is Giving Transaction&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Giving Parent&quot; will not be required field.</inlineHelpText>
        <label>Transaction Required Parent Giving</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Transaction_Type_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Transaction Type&quot; must be &apos;Payment&apos;, &apos;Refund&apos;, or blank.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, will turn of the validation &quot;Transaction_Type_Invalid&quot;.</inlineHelpText>
        <label>Transaction Type Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Transaction_Type_Required_Blank__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Transaction Type&quot; can only be populated on records where &quot;Is Giving Transaction&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Transaction Type&quot; can be populated on all records.</inlineHelpText>
        <label>Transaction Type Required Blank</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Transaction_Type_Required__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Transaction Type&quot; must be populated on records where &quot;Is Giving Transaction&quot; is checked.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Transaction Type&quot; can be left blank on records where &quot;Is Giving Transaction&quot; is checked.</inlineHelpText>
        <label>Transaction Type Required</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Trib_Delivered_Before_Effective_Date__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>&quot;Tribute Delivered Date&quot; cannot be prior to  &quot;Tribute Effective Date&quot;</description>
        <externalId>false</externalId>
        <inlineHelpText>&quot;Tribute Delivered Date&quot; cannot be prior to  &quot;Tribute Effective Date&quot;</inlineHelpText>
        <label>Trib Delivered Before Effective Date</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Tribute_Delivery_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Tribute Delivery&quot; must be &apos;On Air&apos;, &apos;In Person&apos;, &apos;Email&apos;, &apos;Website&apos;, &apos;Other&apos;, or blank.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Tribute Delivery&quot; can hold any value.</inlineHelpText>
        <label>Tribute Delivery Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Tribute_Type_Invalid__c</fullName>
        <defaultValue>true</defaultValue>
        <deprecated>false</deprecated>
        <description>If checked, &quot;Tribute Delivery&quot; must be &apos;On Air&apos;, &apos;In Person&apos;, &apos;Email&apos;, &apos;Website&apos;, &apos;Other&apos; or blank.</description>
        <externalId>false</externalId>
        <inlineHelpText>If unchecked, &quot;Tribute Delivery&quot; can hold any value.</inlineHelpText>
        <label>Tribute Type Invalid</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <label>Opportunity Validation</label>
    <visibility>Public</visibility>
</CustomObject>
