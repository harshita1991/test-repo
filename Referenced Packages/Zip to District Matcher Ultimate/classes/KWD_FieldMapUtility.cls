/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class KWD_FieldMapUtility {
    global static String sFieldNameSpace;
    global static List<String> ZDMFields {
        get;
        set;
    }
    global KWD_FieldMapUtility() {

    }
    global static Boolean CheckFieldLevelAccess(List<String> sFieldList, Map<String,Schema.SObjectField> objectFieldMap) {
        return null;
    }
    global static Schema.DescribeSObjectResult DescribeMapUtility(Schema.SObjectType soType) {
        return null;
    }
    global static Set<String> createUpdateableSet(Schema.DescribeSObjectResult objDescribe) {
        return null;
    }
    global static String getCoreQuery3(List<String> sFields) {
        return null;
    }
    global static String getCustomNameSpace(String soName) {
        return null;
    }
    global static String getmainQuery3(List<String> sFields, String sName) {
        return null;
    }
    global static List<String> getsFieldList(String sName) {
        return null;
    }
}
