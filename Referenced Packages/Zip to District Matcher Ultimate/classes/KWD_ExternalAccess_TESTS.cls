/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class KWD_ExternalAccess_TESTS {
    global KWD_ExternalAccess_TESTS() {

    }
    global static void setZDMMockData(SObject testSo, Boolean bThrowError) {

    }
    global static void setZDMSetupData() {

    }
}
