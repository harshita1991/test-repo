/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class KWD_ExternalAccessWrapper {
    global Boolean bSetKWFields {
        get;
        set;
    }
    global kwzd.KWD_ZDM_Input knowwhoResponse;
    global SObject recordforProcessing;
    global List<SObject> recordsforProcessing;
    global String sErrorCode;
    global List<String> sFields;
    global KWD_ExternalAccessWrapper() {

    }
    global Boolean getKnowWhoData() {
        return null;
    }
    global void updateElectedOfficials() {

    }
    global void updateZDMFields() {

    }
}
