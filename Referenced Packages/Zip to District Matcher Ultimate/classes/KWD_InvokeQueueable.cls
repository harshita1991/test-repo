/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class KWD_InvokeQueueable {
    global KWD_InvokeQueueable() {

    }
    global static Id StartDataProcessQueue() {
        return null;
    }
    global static Id StartDeletionProcessQueue(Id BatchId) {
        return null;
    }
    @Future(callout=false)
    global static void StartDeletionProcessQueueaSync(Id BatchId) {

    }
    global static Id StartProcessQueue(String sInstance) {
        return null;
    }
}
