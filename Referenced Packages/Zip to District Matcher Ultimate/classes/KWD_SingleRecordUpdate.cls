/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class KWD_SingleRecordUpdate {
    global Boolean bSubmittoDatabase {
        get;
        set;
    }
    global String dbError {
        get;
        set;
    }
    global System.StatusCode sc {
        get;
        set;
    }
    global Boolean showaddresserror {
        get;
        set;
    }
    global Boolean showaddressnotfounderror {
        get;
        set;
    }
    global Boolean showcrediterror {
        get;
        set;
    }
    global Boolean showerrors {
        get;
        set;
    }
    global Boolean showexpiryerror {
        get;
        set;
    }
    global Boolean showsaveerror {
        get;
        set;
    }
    global Boolean showziperror {
        get;
        set;
    }
    global SObject soProcessed {
        get;
        set;
    }
    global KWD_SingleRecordUpdate() {

    }
    global void executeupdate(String kwdObjectName, SObject a) {

    }
}
