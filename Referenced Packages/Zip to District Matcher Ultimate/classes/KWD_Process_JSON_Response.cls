/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class KWD_Process_JSON_Response {
    global KWD_Process_JSON_Response() {

    }
    global static SObject updateKWData(SObject kwrecord, kwzd.KWD_ZDM_Input.Records k) {
        return null;
    }
    global static SObject updateKWData2(SObject kwrecord, String sNameSpaceCode, kwzd.KWD_ZDM_Input.Records k) {
        return null;
    }
    global static SObject updateTargetfromSourceObject(SObject sourceSo, String sourceNS, SObject targetSo, String targetNS) {
        return null;
    }
}
