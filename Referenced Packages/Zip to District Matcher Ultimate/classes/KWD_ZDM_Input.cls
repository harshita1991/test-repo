/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class KWD_ZDM_Input {
    global kwzd.KWD_ZDM_Input.KnowWho KnowWho;
    global KWD_ZDM_Input() {

    }
global class Customer {
    global String CreditsAvailable;
    global String CreditsUsed;
    global String ErrorCode;
    global String ExpirationDate;
    global String ReturnCode;
    global Customer() {

    }
}
global class KnowWho {
    global kwzd.KWD_ZDM_Input.Customer Customer;
    global List<kwzd.KWD_ZDM_Input.Records> Records;
    global KnowWho() {

    }
}
global class Records {
    global List<String> AccountIDs;
    global String City;
    global String CountyAccountID;
    global String CountyCode;
    global String CountyDescription;
    global String CountyDistrict;
    global String CountyName;
    global String ErrorCode;
    global String IsPOBox;
    global String Latitude;
    global String Longitude;
    global String MunicipalAccountID;
    global String MunicipalCode;
    global String MunicipalDescription;
    global String MunicipalDistrict;
    global String MunicipalName;
    global String MunicipalType;
    global String Plus4;
    global String SalesforceID;
    global String State;
    global String StateHouseDescription;
    global String StateHouseDistrict;
    global String StateSenateDescription;
    global String StateSenateDistrict;
    global String StreetAddress;
    global String StreetAddress2;
    global String USHouseDescription;
    global String USHouseDistrict;
    global String USPSVerified;
    global String USSenateDescription;
    global String USSenateDistrict;
    global String Zip;
    global Records() {

    }
}
}
