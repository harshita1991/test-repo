<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Mark_for_Deletion</fullName>
        <description>Once Complete, mark for deletion after 5 days to remove storage need</description>
        <field>Status__c</field>
        <literalValue>Delete</literalValue>
        <name>Mark for Deletion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Mark ZDM Staging Records for Deletion</fullName>
        <actions>
            <name>Mark_for_Deletion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Zip_to_District_Data_Staging__c.Status__c</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Delete completed ZDM jobs</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
