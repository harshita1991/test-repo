/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/kwauthsettings')
global class KWDATA_aSyncSettings {
    global KWDATA_aSyncSettings() {

    }
    @HttpPost
    global static void setAPITokens() {

    }
    global static void setupExecSettings(String sNameSpace) {

    }
    global static void setupLocalSettings() {

    }
    global static void setupStateSettings() {

    }
}
