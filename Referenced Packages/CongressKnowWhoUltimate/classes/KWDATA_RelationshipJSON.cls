/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class KWDATA_RelationshipJSON {
    global List<kw.KWDATA_RelationshipJSON.Data> Data;
    global KWDATA_RelationshipJSON() {

    }
    global static kw.KWDATA_RelationshipJSON parse(String json) {
        return null;
    }
global class Data {
    global String AccountID;
    global String AccountSortOrder;
    global String ContactID;
    global String ContactSortOrder;
    global String EnteredBy;
    global String KnowWhoID;
    global String LastEntered;
    global String ParentID;
    global String PersonID;
    global String PrimaryID;
    global String RecordID;
    global String RoleDesc;
    global Data() {

    }
}
}
