/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global virtual class KWDATA_ProcessDataBatchBaseClass {
    global Map<String,kw__KnowWho_Field_Mapping__mdt> cmdFieldMap;
    global Schema.SObjectField kwId;
    global List<Database.UpsertResult> kwResults;
    global List<SObject> newKWRecords;
    global Map<String,Schema.DescribeSObjectResult> schemaResultMap;
    global String sJSON;
    global SObject tempA;
    global KWDATA_ProcessDataBatchBaseClass() {

    }
    global virtual void processRelationships(String sDirectoryType, String skwObjectType, String sStagingId) {

    }
    global Map<String,kw__KnowWho_Field_Mapping__mdt> setupFieldMap(String sDataType, List<String> sDirectoryTypes, String sObjectName) {
        return null;
    }
    global virtual void submitRecordstoDatabase(String sStagingId) {

    }
}
