/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class KWDATA_ProcessSORecord {
    global KWDATA_ProcessSORecord() {

    }
    global static SObject processRecord(SObject tempA, Map<String,Object> m, Map<String,kw__KnowWho_Field_Mapping__mdt> cmdFieldMap) {
        return null;
    }
}
